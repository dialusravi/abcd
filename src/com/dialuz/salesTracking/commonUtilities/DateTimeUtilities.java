package com.dialuz.salesTracking.commonUtilities;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateTimeUtilities
{
	public static String getCurrentDate()
	{
	
		java.text.DateFormat df = new java.text.SimpleDateFormat("dd-MM-yyyy");
		String currentDate=df.format(new Date());
		try{
		String s[]=currentDate.split("-");
		currentDate=s[2]+"-"+s[1]+"-"+s[0];
		}catch(Exception e){}
		return currentDate;
	}
	public static String getCurrentTime()
	{
		Calendar calendar = new GregorianCalendar();
		int hour = calendar.get(Calendar.HOUR);
		int minute = calendar.get(Calendar.MINUTE);
		int second = calendar.get(Calendar.SECOND);
		String am_pm;
		if(calendar.get(Calendar.AM_PM) == 0)
			   am_pm = "AM";
			else
			   am_pm = "PM";
			String currentTime = hour+":"+ minute +":"+ second +" "+ am_pm;
		return currentTime;
	}
	public static String stringArrayToString(String name[])
	{
		
		if (name.length > 0) {
		    StringBuilder nameBuilder = new StringBuilder();

		    for (String n : name) {
		        nameBuilder.append("").append(n.replaceAll("'", "\\\\'")).append(",");
		        // can also do the following
		        // nameBuilder.append("'").append(n.replaceAll("'", "''")).append("',");
		    }

		    nameBuilder.deleteCharAt(nameBuilder.length() - 1);

		    return nameBuilder.toString();
		} else {
		    return "";
		}
	}
	public static String getLocalTime(String dbTime)
	{
		String localTime="";
		String session="";
		int hour=0;
		String time[]=null;
		try{
			time=dbTime.split(":");
			try{
				if(Integer.parseInt(time[0])>=12)
				{
					session="PM";
					hour=24-Integer.parseInt(time[0]);
				}else
				{
					session="AM";
					hour=Integer.parseInt(time[0]);
				}
			}catch (Exception e) {e.printStackTrace();
			}
		}catch (Exception e) {
		}
		
		localTime=hour+":"+time[1]+":"+time[2]+" "+session;
		return localTime;
	}

}
