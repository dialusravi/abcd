package com.dialuz.salesTracking.DTO;

import java.io.Serializable;


public class SelectSubCategoryDTO implements Serializable 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1047960506827464822L;
	/**
	 * 
	 */
	/**
	 * 
	 */
	private int catid;
	public int getCatid() {
		return catid;
	}
	public void setCatid(int catid) {
		this.catid = catid;
	}
	
	private String catName;
	private String subCategoryName;
	private String subSubCategoryName;
	public String getCatName() {
		return catName;
	}
	public void setCatName(String catName) {
		this.catName = catName;
	}
	public String getSubCategoryName() {
		return subCategoryName;
	}
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}
	public String getSubSubCategoryName() {
		return subSubCategoryName;
	}
	public void setSubSubCategoryName(String subSubCategoryName) {
		this.subSubCategoryName = subSubCategoryName;
	}
	
	

}


