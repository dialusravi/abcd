package com.dialuz.salesTracking.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.dialuz.salesTracking.DB.DBConfig;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.dialuz.salesTracking.DTO.LoginDTO;

public class LoginDAO 
{

	public LoginDTO checkLoginDetails(LoginDTO objLoginDTO)
	{
		LoginDTO objLoginDTO1=new LoginDTO();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String query="";
		try{
				objConnection=DBConfig.connect();
				
				if(objLoginDTO.getUsertype()==null || objLoginDTO.getUsertype()=="" || objLoginDTO.getUsertype().isEmpty())
				{
					
				}
				else
				{
				
				if(objLoginDTO.getUsertype().equals("telecaller"))
				{
					query="select telecaller_username,telecaller_password,usertype,telecaller_mobileno  from telecaller_details_arabic  where telecaller_username=? and telecaller_password=? and status_flag=0 ";
					objPreparedStatement=objConnection.prepareStatement(query);
					objPreparedStatement.setString(1, objLoginDTO.getUsername());
					objPreparedStatement.setString(2, objLoginDTO.getPassword());
					objResultSet=objPreparedStatement.executeQuery();
					if(objResultSet.next())
					{
						objLoginDTO1.setDbUserType((objResultSet.getString("usertype")));
						objLoginDTO1.setUsername(objResultSet.getString("telecaller_username"));
						objLoginDTO1.setUserMobile(objResultSet.getString("telecaller_mobileno"));
						objLoginDTO1.setLoginStatus(true);
								
					}else
					{
						objLoginDTO1.setLoginStatus(false);
					}
				
				}else if(objLoginDTO.getUsertype().equals("sales"))
				{
					query="select mobile_no,password,usertype  from   sales_user_details_arabic   where mobile_no=? and password=? and status_flag=0";
					objPreparedStatement=objConnection.prepareStatement(query);
					objPreparedStatement.setString(1, objLoginDTO.getUsername());
					objPreparedStatement.setString(2, objLoginDTO.getPassword());
					objResultSet=objPreparedStatement.executeQuery();
					if(objResultSet.next())
					{
						objLoginDTO1.setDbUserType((objResultSet.getString("usertype")));
						objLoginDTO1.setUsername(objResultSet.getString("mobile_no"));
						objLoginDTO1.setLoginStatus(true);
								
					}else
					{
						
						objLoginDTO1.setLoginStatus(false);
					}
				
				}else
				{
					query="select username,password,usertype from admin_login_details_arabic   where username=? and password=? ";
					
					objPreparedStatement=objConnection.prepareStatement(query);
					objPreparedStatement.setString(1, objLoginDTO.getUsername());
					objPreparedStatement.setString(2, objLoginDTO.getPassword());
					objResultSet=objPreparedStatement.executeQuery();
					
					if(objResultSet.next())
					{
						objLoginDTO1.setDbUserType((objResultSet.getString("usertype")));
						objLoginDTO1.setUsername(objResultSet.getString("username"));
						objLoginDTO1.setLoginStatus(true);
								
					}else
					{
						objLoginDTO1.setLoginStatus(false);
					}
				
				}}
				
		}catch(Exception e)
		{
			
		}finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
			
		return objLoginDTO1;
	}

	public ArrayList<String> getTelecallerNames() 
	{
		ArrayList<String> TelecallerNames=new ArrayList<String>();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql="select telecaller_name from telecaller_details_arabic where status_flag=0";
		try{
			objConnection=DBConfig.connect();
			objPreparedStatement=objConnection.prepareStatement(sql);
			objResultSet=objPreparedStatement.executeQuery();
			while(objResultSet.next())
			{
				TelecallerNames.add(objResultSet.getString("telecaller_name"));
			}
			
		}catch (Exception e) {
		}finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		
		
		return TelecallerNames;
	}

	public ArrayList<AppointmentDTO> getSalesPersonNames() 
	{
		ArrayList<AppointmentDTO> salesPersonNames=new ArrayList<AppointmentDTO>();
		
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql="select salesperson_name,mobile_no  from sales_user_details_arabic  where status_flag=0";
		try{
			objConnection=DBConfig.connect();
			objPreparedStatement=objConnection.prepareStatement(sql);
			objResultSet=objPreparedStatement.executeQuery();
			while(objResultSet.next())
			{
				AppointmentDTO objAppointmentDTO=new AppointmentDTO();
				objAppointmentDTO.setSalesPersonName(objResultSet.getString("salesperson_name"));
				objAppointmentDTO.setSalesPersonContactNo(objResultSet.getString("mobile_no"));
				salesPersonNames.add(objAppointmentDTO);
			}
			
		}catch (Exception e) {
		}finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		
		
		return salesPersonNames;
	}

}
