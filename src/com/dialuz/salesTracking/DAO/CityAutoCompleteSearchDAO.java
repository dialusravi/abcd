package com.dialuz.salesTracking.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.dialuz.salesTracking.DB.DBConfig;

public class CityAutoCompleteSearchDAO {
	
	public ArrayList<String> getautoCompleteCitiesList(String q,String businessState)
	{

		  ArrayList<String> SearchCityList=new ArrayList<String>();
			Connection objConnection = null;
			PreparedStatement objPreparedStatement = null;
			ResultSet objResultSet = null;
			
			 try {
					 objConnection = DBConfig.connectToMainDB();
					 String query="SELECT distinct city FROM  india_cities_SA   where  city like '"+q+"%' and state='"+businessState+"'  LIMIT 05 ";
					 objPreparedStatement = objConnection.prepareStatement(query);
					
					 objResultSet = objPreparedStatement.executeQuery();
					 
					while(objResultSet.next())
					 {
						
						SearchCityList.add(objResultSet.getString("city"));
			    			
					 }
					
			 } 
			 catch (Exception e) {
				e.printStackTrace();
			 } 
			 finally {
				
				 DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);

			 }

			 return SearchCityList;
		
	}
	
public ArrayList<String> getautoCompleteNames(String q)
{

		  ArrayList<String> NameList=new ArrayList<String>();
			Connection objConnection = null;
			PreparedStatement objPreparedStatement = null;
			ResultSet objResultSet = null;
			
			 try {
					 objConnection = DBConfig.connect();
					 String query="SELECT distinct salesPersonName FROM  appointment_details_arabic   where  salesPersonName like '"+q+"%'  LIMIT 05 ";
					 objPreparedStatement = objConnection.prepareStatement(query);
					
					 objResultSet = objPreparedStatement.executeQuery();
					 
					while(objResultSet.next())
					 {
						
						NameList.add(objResultSet.getString("salesPersonName"));
			    			
					 }
					
			 } 
			 catch (Exception e) {
				e.printStackTrace();
			 } 
			 finally {
				
				 DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);

			 }

			 return NameList;
		
}
	
public ArrayList<String> getautoCompleteStateList(String q)
{
		ArrayList<String> SearchCityList=new ArrayList<String>();
		Connection objConnection = null;
		PreparedStatement objPreparedStatement = null;
		ResultSet objResultSet = null;
		
		 try {
				 objConnection = DBConfig.connectToMainDB();
				 String query="SELECT  distinct state FROM india_cities_SA  where  state  like '"+q+"%'  LIMIT 05 ";
				
				 objPreparedStatement = objConnection.prepareStatement(query);
				
				 objResultSet = objPreparedStatement.executeQuery();
				 
				while(objResultSet.next())
				 {
					
					SearchCityList.add(objResultSet.getString("state"));
		    			
				 }
				
		 } 
		 catch (Exception e) {
			e.printStackTrace();
		 } 
		 finally {
	
			 DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);

		 }

		 return SearchCityList;
	}
	
	
	public ArrayList<String> getautoCompleteAreaList(String q, String businesCity) 
	{
		ArrayList<String> SearchCityList=new ArrayList<String>();
		Connection objConnection = null;
		PreparedStatement objPreparedStatement = null;
		ResultSet objResultSet = null;
		
		 try {
				 objConnection = DBConfig.connectToMainDB();
				 String query="SELECT  distinct business_area FROM area_SA  where  business_area  like '"+q+"%' and business_city='"+businesCity+"' LIMIT 05";
				 
				 objPreparedStatement = objConnection.prepareStatement(query);
				
				 objResultSet = objPreparedStatement.executeQuery();
				 
				while(objResultSet.next())
				 {
					
					SearchCityList.add(objResultSet.getString("business_area"));
		    			
				 }
				
		 } 
		 catch (Exception e) {
			e.printStackTrace();
		 } 
		 finally {

			 DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);

		 }

		 return SearchCityList;
	}
public ArrayList<String> getautoCompleteArabicAreaList(String q, String businesCity)
{
		ArrayList<String> SearchCityList=new ArrayList<String>();
		Connection objConnection = null;
		PreparedStatement objPreparedStatement = null;
		ResultSet objResultSet = null;
		
		
		
		
		 try {
			 try{
				 	q=q.trim();
				 }catch (Exception e) {
				}
				 objConnection = DBConfig.connectArabicDb();
				 String query="SELECT  distinct business_area FROM business_master    where  business_area  like '"+q+"%' and business_city='"+businesCity+"' LIMIT 0,5  ";
				
				 objPreparedStatement = objConnection.prepareStatement(query);
				
				 objResultSet = objPreparedStatement.executeQuery();
				 
				while(objResultSet.next())
				 {
					
					SearchCityList.add(objResultSet.getString("business_area"));
		    			
				 }
				
		 } 
		 catch (Exception e) {
			e.printStackTrace();
		 } 
		 finally {

			 DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);

		 }

		 return SearchCityList;
}
public ArrayList<String> getautoCompleteArabicCitiesList(String q,String businessState) {

	  ArrayList<String> SearchCityList=new ArrayList<String>();
		Connection objConnection = null;
		PreparedStatement objPreparedStatement = null;
		ResultSet objResultSet = null;
		
		
		
		
		 try {
			 try{
				 	q=q.trim();
				 }catch (Exception e) {
				}
			 	
				 objConnection = DBConfig.connectArabicDb();
				 String query="SELECT distinct business_city FROM business_master  where  business_city like '"+q+"%' LIMIT 05 ";
				 objPreparedStatement = objConnection.prepareStatement(query);
				
				 objResultSet = objPreparedStatement.executeQuery();
				 
				while(objResultSet.next())
				 {
					
					SearchCityList.add(objResultSet.getString("business_city"));
		    			
				 }
				
		 } 
		 catch (Exception e) {
			e.printStackTrace();
		 } 
		 finally {
			
			 DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);

		 }

		 return SearchCityList;
	
}
	
	
	
	
	
	
	
	
	


}
