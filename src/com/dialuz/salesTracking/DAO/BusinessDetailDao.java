package com.dialuz.salesTracking.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.dialuz.salesTracking.DB.DBConfig;
import com.dialuz.salesTracking.DTO.BusinessDetailDTO;
import com.dialuz.salesTracking.DTO.TrackFieldDTO;

public class BusinessDetailDao {

	public ArrayList<TrackFieldDTO> getTrackFieldSales() {

		ArrayList<TrackFieldDTO> objArrayList = new ArrayList<TrackFieldDTO>();
		Connection objConnection = null;
		PreparedStatement objPreparedStatement = null;
		PreparedStatement objPreparedStatement1 = null;
		ResultSet objResultSet = null;
		ResultSet objResultSet1 = null;
		try {
			objConnection = DBConfig.connect();

			String sql12 = "SELECT * FROM tracking_arabic t LIMIT 0,50";

			objPreparedStatement1 = objConnection.prepareStatement(sql12);

			objResultSet1 = objPreparedStatement1.executeQuery();

			while (objResultSet1.next()) {
				TrackFieldDTO objTrackFieldDTO=new TrackFieldDTO();
                 
				objTrackFieldDTO.setId(objResultSet1.getInt("tracking_id"));
				objTrackFieldDTO.setBusiness_name(objResultSet1.getString("business_name"));
				objTrackFieldDTO.setMobile(objResultSet1.getString("business_mobile"));
				objTrackFieldDTO.setCity(objResultSet1.getString("business_city"));
				objTrackFieldDTO.setArea(objResultSet1.getString("business_area"));
				objTrackFieldDTO.setLongitude(objResultSet1.getString("business_longitude"));
				objTrackFieldDTO.setLatitude(objResultSet1.getString("business_latitude"));
				
				objArrayList.add(objTrackFieldDTO);

			}

			

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			DBConfig.disconnect(objConnection, objPreparedStatement,
					objResultSet);

		}

		return objArrayList;

	}
	
	public int deleteDailyPlan(int business_id)
	{
		Connection objConnection = null;
		PreparedStatement objPreparedStatement = null;
		int result = 0;
		try {
			objConnection = DBConfig.connectToMainDB();
			String query1 = "update business_detail_SA set status=1 where business_id=?";
			objPreparedStatement = objConnection.prepareStatement(query1);
			objPreparedStatement.setInt(1, business_id);
			result = objPreparedStatement.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			DBConfig.disconnect(objConnection, objPreparedStatement, null);

		}
		return result;
	}
	
	public int deleteArabicDailyPlan(int business_id)
	{
		Connection objConnection = null;
		PreparedStatement objPreparedStatement = null;
		int result = 0;
		try {
			objConnection = DBConfig.connectArabicDb();
			String query1 = "update business_detail set status=1 where business_id=?";
			objPreparedStatement = objConnection.prepareStatement(query1);
			objPreparedStatement.setInt(1, business_id);
			result = objPreparedStatement.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			DBConfig.disconnect(objConnection, objPreparedStatement, null);

		}
		return result;
	}
	
	
//For Dialy bCall plan start
	public ArrayList<BusinessDetailDTO> getDailyCallplan(String city)
	{

		ArrayList<BusinessDetailDTO> objArrayList = new ArrayList<BusinessDetailDTO>();
		Connection objConnection = null;
		PreparedStatement objPreparedStatement = null;
		PreparedStatement objPreparedStatement1 = null;
		ResultSet objResultSet = null;
		ResultSet objResultSet1 = null;
		try {
			objConnection = DBConfig.connect();

			String sql12 ="SELECT  business_mobile FROM appointment_details_english where business_mobile is not null and business_city=? and business_mobile!='' ";

			objPreparedStatement1 = objConnection.prepareStatement(sql12);
			objPreparedStatement1.setString(1, city);

			objResultSet1 = objPreparedStatement1.executeQuery();

			ArrayList<String> strArrayList = new ArrayList<String>();

			while (objResultSet1.next())
			{

				//objResultSet1.getString("business_mobile");

				strArrayList.add(objResultSet1.getString("business_mobile"));

			}
			if(strArrayList.size()==0)
			{
				strArrayList.add("0000000000");
			}

			objArrayList = getAll(strArrayList,city);


		} catch (Exception e)
		{
			e.printStackTrace();
		} finally
		{

			DBConfig.disconnect(objConnection, objPreparedStatement,objResultSet);

		}

		return objArrayList;

	}
	public ArrayList<BusinessDetailDTO> getAll(ArrayList<String> strArrayList ,String city)
	{

			ArrayList<BusinessDetailDTO> objArrayList = new ArrayList<BusinessDetailDTO>();
			Connection objConnection = null;
			PreparedStatement objPreparedStatement = null;
			PreparedStatement objPreparedStatement1 = null;
			ResultSet objResultSet = null;
			ResultSet objResultSet1 = null;
			try {
				objConnection = DBConfig.connectToMainDB();
				String all = strArrayList.toString().replace("[", "(").replace("]",")");
				

			  	String sql12="SELECT a.business_id,business_name,business_mob1,business_email,b.business_city,b.keyword1 FROM business_detail_SA a left join business_master_SA b on b.business_id=a.business_id where a.business_mob1 not in "+all+" and  a.business_email is not null and b.business_city=? and a.business_mob1 is not null and status=0 limit 0,100";

				objPreparedStatement1 = objConnection.prepareStatement(sql12);
				objPreparedStatement1.setString(1, city);

				objResultSet1 = objPreparedStatement1.executeQuery();

				   while (objResultSet1.next()) {
					BusinessDetailDTO objBusinessDetailDTO = new BusinessDetailDTO();

					objBusinessDetailDTO.setBusiness_id(objResultSet1
							.getInt("business_id"));
					objBusinessDetailDTO.setCity(objResultSet1.getString("business_city"));
					objBusinessDetailDTO.setKeyword1(objResultSet1.getString("keyword1"));
					objBusinessDetailDTO.setBusiness_name(objResultSet1
							.getString("business_name"));
					objBusinessDetailDTO.setMobile(objResultSet1
							.getString("business_mob1"));
					objBusinessDetailDTO.setEmail(objResultSet1
							.getString("business_email"));

					objArrayList.add(objBusinessDetailDTO);

				}

			} catch (Exception e) {
				e.printStackTrace();
			} finally {

				DBConfig.disconnect(objConnection, objPreparedStatement,
						objResultSet);

			}

			return objArrayList;

		}
		
	
	
	
//dialycallpaln end	
	
	//start arabic call back
	
	public ArrayList<BusinessDetailDTO> getArabicDailyCallplan(String city) {
		
		
		ArrayList<BusinessDetailDTO> objArrayList = new ArrayList<BusinessDetailDTO>();
		Connection objConnection = null;
		PreparedStatement objPreparedStatement = null;
		PreparedStatement objPreparedStatement1 = null;
		ResultSet objResultSet = null;
		ResultSet objResultSet1 = null;
		try {
			objConnection = DBConfig.connect();

			String sql12 ="SELECT  business_mobile FROM appointment_details_arabic where business_mobile is not null and business_city=? and business_mobile!='' ";

			objPreparedStatement1 = objConnection.prepareStatement(sql12);
			objPreparedStatement1.setString(1, city);

			objResultSet1 = objPreparedStatement1.executeQuery();

			ArrayList<String> strArrayList = new ArrayList<String>();

			while (objResultSet1.next())
			{

				//objResultSet1.getString("business_mobile");

				strArrayList.add(objResultSet1.getString("business_mobile"));

			}
			if(strArrayList.size()==0)
			{
				strArrayList.add("0000000000");
			}

			objArrayList = getArabicAll(strArrayList,city);


		} catch (Exception e)
		{
			e.printStackTrace();
		} finally
		{

			DBConfig.disconnect(objConnection, objPreparedStatement,objResultSet);

		}

		return objArrayList;
	}

	public ArrayList<BusinessDetailDTO> getArabicAll(ArrayList<String> strArrayList,String city) {
		System.out.println("in "+city);
		ArrayList<BusinessDetailDTO> objArrayList = new ArrayList<BusinessDetailDTO>();
		Connection objConnection = null;
		PreparedStatement objPreparedStatement = null;
		PreparedStatement objPreparedStatement1 = null;
		ResultSet objResultSet = null;
		ResultSet objResultSet1 = null;
		try {
			objConnection = DBConfig.connectArabicDb();
			String all = strArrayList.toString().replace("[", "(").replace("]",")");
			

		  	String sql12="SELECT a.business_id,business_name,business_mob1,business_email,b.business_city,b.keyword1 FROM business_detail a left join business_master b on b.business_id=a.business_id where a.business_mob1 not in "+all+" and  a.business_email is not null and b.business_city=? and a.business_mob1 is not null and status=0 limit 0,100";

			objPreparedStatement1 = objConnection.prepareStatement(sql12);
			objPreparedStatement1.setString(1, city);

			objResultSet1 = objPreparedStatement1.executeQuery();
			 System.out.println("in whileee"+objResultSet1);
			   while (objResultSet1.next()) {
				  
				BusinessDetailDTO objBusinessDetailDTO = new BusinessDetailDTO();

				objBusinessDetailDTO.setBusiness_id(objResultSet1
						.getInt("business_id"));
				objBusinessDetailDTO.setCity(objResultSet1.getString("business_city"));
				objBusinessDetailDTO.setKeyword1(objResultSet1.getString("keyword1"));
				objBusinessDetailDTO.setBusiness_name(objResultSet1
						.getString("business_name"));
				objBusinessDetailDTO.setMobile(objResultSet1
						.getString("business_mob1"));
				objBusinessDetailDTO.setEmail(objResultSet1
						.getString("business_email"));

				objArrayList.add(objBusinessDetailDTO);
				System.out.println(objArrayList.size());

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			DBConfig.disconnect(objConnection, objPreparedStatement,
					objResultSet);

		}
		System.out.println("listtttttttttt"+objArrayList.size());
		return objArrayList;

	}
	
	
	
	//end dialycallback
	
	
	
	
	
	
	
	public ArrayList getBusinessDetail() {
		
		ArrayList objArrayList=new ArrayList();
		Connection objConnection = null;
		PreparedStatement objPreparedStatement = null;
		PreparedStatement objPreparedStatement1 = null;
		ResultSet objResultSet = null;
		ResultSet objResultSet1 = null;
		 try {
			 objConnection = DBConfig.connectToMainDB();
			// String query="select * from userinfo_arabic where status=0";
			// objPreparedStatement = objConnection.prepareStatement(query);
			 //objResultSet = objPreparedStatement.executeQuery();
			 
		//	while(objResultSet.next())
			// {
				
				/*BusinessDetailDTO objBusinessDetailDTO=new BusinessDetailDTO();
				
				objBusinessDetailDTO.setName(objResultSet.getString("name"));
				objBusinessDetailDTO.setMobile(objResultSet.getString("mobile"));
				objBusinessDetailDTO.setEmail(objResultSet.getString("email"));*/
				
			//	if(objResultSet.getInt("business_id")!=0)
			//	{
				//String query1="select business_id,business_name,business_city,business_area,business_cat from business_master_arabic where business_id in (selec";	
							
				String sql12="SELECT id,name,mobile,email,date_time,business_master_SA.business_id,business_master_SA.business_name,business_master_SA.business_city,business_master_SA.business_area,business_master_SA.keyword1 FROM userinfo_SA inner join business_master_SA on userinfo_SA.business_id=business_master_SA.business_id where userinfo_SA.status=0 order by id desc";	
					
					objPreparedStatement1 = objConnection.prepareStatement(sql12);
					
					//objPreparedStatement1.setInt(1,objResultSet.getInt("business_id"));
					
					 objResultSet1 = objPreparedStatement1.executeQuery();
					 
					 while(objResultSet1.next())
					 {
						 BusinessDetailDTO objBusinessDetailDTO=new BusinessDetailDTO();
						 
						 objBusinessDetailDTO.setId(objResultSet1.getInt("id"));
						 objBusinessDetailDTO.setName(objResultSet1.getString("name"));
						 objBusinessDetailDTO.setMobile(objResultSet1.getString("mobile"));
						 objBusinessDetailDTO.setEmail(objResultSet1.getString("email"));
						
						
						 
						 
						 
						 
						 
						 objBusinessDetailDTO.setSt(objResultSet1.getTimestamp("date_time"));
						 
						 
						 
						 objBusinessDetailDTO.setCity(objResultSet1.getString("business_city")); 
						 objBusinessDetailDTO.setArea(objResultSet1.getString("business_area"));
						 objBusinessDetailDTO.setCat(objResultSet1.getString("keyword1"));
						 objBusinessDetailDTO.setBusiness_id(objResultSet1.getInt("business_id"));
						 objBusinessDetailDTO.setBusiness_name(objResultSet1.getString("business_name"));
						
						 
						
						 objArrayList.add(objBusinessDetailDTO);
					 
					 }
					
					
					 	
				//}
				
			// }
			
	 } 
	 catch (Exception e) 
	 {
		e.printStackTrace();
	 } 
	 finally
	 {
		
		 DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);

	 		}
			
	
		return objArrayList;
		
  }

	public ArrayList getBusinessPriority(String businessCat,String business_city) 
	{
		
		Connection objConnection = null;
		PreparedStatement objPreparedStatement = null;
		
		ResultSet objResultSet = null;
		
		ArrayList objArrayList=new ArrayList();
		
		
		
		try {
			 objConnection = DBConfig.connectToMainDB();
					 
			// String query1="select business_email,business_name,business_mob1,business_master_arabic.business_priority from business_detail_arabic inner join business_master_arabic on business_detail_arabic.business_id=business_master_arabic.business_id where business_master_arabic.business_priority between 7 and 10 AND business_master_arabic.business_cat=? and business_master_arabic.business_city=?";	
			 
			String query1 = "select m.business_name,m.business_id,n.business_mob1," +
						" n.business_email, m.business_priority " +
						" from business_master_SA m inner join business_detail_SA  n on  " +
						" m.business_id = n.business_id   where " +
						" m.business_city=?   and  " +
						"(m.keyword1='"+businessCat+"' OR m.keyword1 like '"+businessCat+" %'  OR  m.keyword1 like '% "+businessCat+"' OR " +
				   		"m.keyword2='"+businessCat+"' OR m.keyword2 like '"+businessCat+" %'  OR  m.keyword2 like '% "+businessCat+"' OR " +
				   		"m.keyword3='"+businessCat+"' OR m.keyword3 like '"+businessCat+" %'  OR  m.keyword3 like '% "+businessCat+"' OR " +
				   		"m.keyword4='"+businessCat+"' OR m.keyword4 like '"+businessCat+" %'  OR  m.keyword4 like '% "+businessCat+"' OR " +
				   		"m.keyword5='"+businessCat+"' OR m.keyword5 like '"+businessCat+" %'  OR  m.keyword5 like '% "+businessCat+"' OR " +
				   		"m.keyword6='"+businessCat+"' OR m.keyword6 like '"+businessCat+" %'  OR  m.keyword6 like '% "+businessCat+"' OR " +
				   	    "m.keyword7='"+businessCat+"' OR m.keyword7 like '"+businessCat+" %'  OR  m.keyword7 like '% "+businessCat+"' OR " +
				   		"m.keyword8='"+businessCat+"' OR m.keyword8 like '"+businessCat+" %'  OR  m.keyword8 like '% "+businessCat+"' OR " +
				   		"m.keyword9='"+businessCat+"' OR m.keyword9 like '"+businessCat+" %'  OR  m.keyword9 like '% "+businessCat+"' OR " +
				   		"m.keyword10='"+businessCat+"' OR m.keyword10 like '"+businessCat+" %'  OR  m.keyword10 like '% "+businessCat+"' OR " +		
						"m.keyword11='"+businessCat+"' OR m.keyword11 like '"+businessCat+" %'  OR  m.keyword11 like '% "+businessCat+"' OR " +
						"m.keyword12='"+businessCat+"' OR m.keyword12 like '"+businessCat+" %'  OR  m.keyword12 like '% "+businessCat+"' OR " +
						"m.keyword13='"+businessCat+"' OR m.keyword13 like '"+businessCat+" %'  OR  m.keyword13 like '% "+businessCat+"' OR " +
						"m.keyword14='"+businessCat+"' OR m.keyword14 like '"+businessCat+" %'  OR  m.keyword14 like '% "+businessCat+"' OR " +
						"m.keyword15='"+businessCat+"' OR m.keyword15 like '"+businessCat+" %'  OR  m.keyword15 like '% "+businessCat+"' OR " +
						"m.keyword16='"+businessCat+"' OR m.keyword16 like '"+businessCat+" %'  OR  m.keyword16 like '% "+businessCat+"' OR " +
						"m.keyword17='"+businessCat+"' OR m.keyword17 like '"+businessCat+" %'  OR  m.keyword17 like '% "+businessCat+"' OR " +
						"m.keyword18='"+businessCat+"' OR m.keyword18 like '"+businessCat+" %'  OR  m.keyword18 like '% "+businessCat+"' OR " +
						"m.keyword19='"+businessCat+"' OR m.keyword19 like '"+businessCat+" %'  OR  m.keyword19 like '% "+businessCat+"' OR " +
						"m.keyword20='"+businessCat+"' OR m.keyword20 like '"+businessCat+" %'  OR  m.keyword20 like '% "+businessCat+"' OR " +
						"m.keyword21='"+businessCat+"' OR m.keyword21 like '"+businessCat+" %'  OR  m.keyword21 like '% "+businessCat+"' OR " +
						"m.keyword22='"+businessCat+"' OR m.keyword22 like '"+businessCat+" %'  OR  m.keyword22 like '% "+businessCat+"' OR " +
						"m.keyword23='"+businessCat+"' OR m.keyword23 like '"+businessCat+" %'  OR  m.keyword23 like '% "+businessCat+"' OR " +
						"m.keyword24='"+businessCat+"' OR m.keyword24 like '"+businessCat+" %'  OR  m.keyword24 like '% "+businessCat+"' OR " +
						"m.keyword25='"+businessCat+"' OR m.keyword25 like '"+businessCat+" %'  OR  m.keyword25 like '% "+businessCat+"' OR " +
						"m.keyword26='"+businessCat+"' OR m.keyword26 like '"+businessCat+" %'  OR  m.keyword26 like '% "+businessCat+"' OR " +
						"m.keyword27='"+businessCat+"' OR m.keyword27 like '"+businessCat+" %'  OR  m.keyword27 like '% "+businessCat+"' OR " +
						"m.keyword28='"+businessCat+"' OR m.keyword28 like '"+businessCat+" %'  OR  m.keyword28 like '% "+businessCat+"' OR " +
						"m.keyword29='"+businessCat+"' OR m.keyword29 like '"+businessCat+" %'  OR  m.keyword29 like '% "+businessCat+"' OR " +
						"m.keyword30='"+businessCat+"' OR m.keyword30 like '"+businessCat+" %'  OR  m.keyword30 like '% "+businessCat+"') " +
						" and  m.business_priority>4"; 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 objPreparedStatement = objConnection.prepareStatement(query1);
			 objPreparedStatement.setString(1,business_city);
			// objPreparedStatement.setString(2,business_city);
			 objResultSet = objPreparedStatement.executeQuery();
			  
			  while(objResultSet.next())
			 {	
				  BusinessDetailDTO objBusinessDetailDTO=new BusinessDetailDTO();	
				  objBusinessDetailDTO.setMobile(objResultSet.getString("business_mob1"));
				  objBusinessDetailDTO.setEmail(objResultSet.getString("business_email"));	
				  objBusinessDetailDTO.setBusiness_name(objResultSet.getString("business_name"));	
				  
				  
				  objArrayList.add(objBusinessDetailDTO);
			 }
			
			
	 } 
	 catch (Exception e) {
		e.printStackTrace();
	 } 
	 finally {
		
		 DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);

	 }
			
	
		
		
		
		
		return objArrayList;
	}

	public int deleteBusinessDetail(int business_id,int id) 
	{
		Connection objConnection = null;
		PreparedStatement objPreparedStatement = null;
		int result=0;
		try {
			 objConnection = DBConfig.connectToMainDB();
			 String query1="update userinfo_SA set status=2 where id=?"; 
			 objPreparedStatement=objConnection.prepareStatement(query1);
			 objPreparedStatement.setInt(1,id);
			 result=objPreparedStatement.executeUpdate();
			
			
	        } 
	 catch (Exception e) 
	 {
		e.printStackTrace();
	 } 
	 finally
	 {
		
		 DBConfig.disconnect(objConnection, objPreparedStatement, null);

	 }
		return result;
	}

	public void insertSmsContent(String business_name, String mobile,int business_id, String msg_content)
	{
		
		
		
		Connection objConnection = null;
		PreparedStatement objPreparedStatement = null;
		int result=0;
		try {
			 objConnection = DBConfig.connect();
			 String query1="insert into business_smscontent_arabic(business_id,business_name,mobile,sms_content) values(?,?,?,?)"; 
			 objPreparedStatement=objConnection.prepareStatement(query1);
			 objPreparedStatement.setInt(1, business_id);
			 objPreparedStatement.setString(2, business_name);
			 objPreparedStatement.setString(3, mobile);
			 objPreparedStatement.setString(4, msg_content);
			
			 result=objPreparedStatement.executeUpdate();
			 
			
						
	        } 
	 catch (Exception e) 
	 {
		e.printStackTrace();
	 } 
	 finally
	 {
		
		 DBConfig.disconnect(objConnection, objPreparedStatement, null);

	 }
	
		
	}

	public ArrayList getPriority(String cat)
	{
		

		Connection objConnection = null;
		PreparedStatement objPreparedStatement1 = null;
		ResultSet objResultSet1 = null;
		ArrayList objArrayList1=new ArrayList();
		try{
			
			objConnection = DBConfig.connectToMainDB();
			 String query="select business_master_SA.business_id,business_master_SA.business_priority from business_detail_SA inner join business_master_SA on business_detail_SA.business_id=business_master_SA.business_id and business_master_SA.business_priority>=4 AND business_master_SA.business_cat=?";	
			 objPreparedStatement1=objConnection.prepareStatement(query);
			 objPreparedStatement1.setString(1, cat);
			 objResultSet1=objPreparedStatement1.executeQuery();
			 while(objResultSet1.next())
			 { 
				 BusinessDetailDTO objBusinessDetailDTO=new BusinessDetailDTO();
				 objBusinessDetailDTO.setPriority(objResultSet1.getInt("business_priority"));
				
				 objArrayList1.add(objBusinessDetailDTO);
				 // objArrayList1.add(objResultSet1.getInt("business_priority"));
			 }
	
			 
		}catch (Exception e)
		{
			e.printStackTrace();
		}finally{
			DBConfig.disconnect(objConnection, objPreparedStatement1, objResultSet1);
		}
		return objArrayList1;
	}

	public ArrayList getBusinessPriority4(String cat,String business_city) {

		Connection objConnection = null;
		PreparedStatement objPreparedStatement = null;
		
		ResultSet objResultSet = null;
		
		ArrayList objArrayList=new ArrayList();
			
		
		try {
			 objConnection = DBConfig.connectToMainDB();
					 
			 String query1="select business_email,business_name,business_mob1,business_master_SA.business_priority from business_detail_SA inner join business_master_SA on business_detail_SA.business_id=business_master_SA.business_id where business_master_SA.business_priority between 4 and 6 AND business_master_SA.business_cat=? and business_master_SA.business_city=? ";	
			 objPreparedStatement = objConnection.prepareStatement(query1);
			 objPreparedStatement.setString(1,cat);
			 objPreparedStatement.setString(2,business_city);
			 objResultSet = objPreparedStatement.executeQuery();
			  
			  while(objResultSet.next())
			 {	
				  BusinessDetailDTO objBusinessDetailDTO=new BusinessDetailDTO();	
				  objBusinessDetailDTO.setMobile(objResultSet.getString("business_mob1"));
				  objBusinessDetailDTO.setEmail(objResultSet.getString("business_email"));	
				  objBusinessDetailDTO.setBusiness_name(objResultSet.getString("business_name"));	
					
				  
				  
				  objArrayList.add(objBusinessDetailDTO);
			 }
	
			
	 } 
	 catch (Exception e) {
		e.printStackTrace();
	 } 
	 finally {
		
		 DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);

	 }
			
	
		
		
		return objArrayList;
	}

	public ArrayList getAllSms() {
		Connection objConnection = null;
	PreparedStatement objPreparedStatement = null;
	
	ResultSet objResultSet = null;
	
	ArrayList objArrayList=new ArrayList();
	
	
	
	try {
		 objConnection = DBConfig.connect();
				 
		 String query1="select * from business_smscontent_arabic order by sms_id desc limit 0,100";	
		 objPreparedStatement = objConnection.prepareStatement(query1);
		
		 objResultSet = objPreparedStatement.executeQuery();
		  
		  while(objResultSet.next())
		 {	
			  BusinessDetailDTO objBusinessDetailDTO=new BusinessDetailDTO();	
			  objBusinessDetailDTO.setSms_id(objResultSet.getInt("sms_id"));
			  objBusinessDetailDTO.setBusiness_id(objResultSet.getInt("business_id"));
			  objBusinessDetailDTO.setBusiness_name(objResultSet.getString("business_name"));
			  objBusinessDetailDTO.setMobile(objResultSet.getString("mobile"));
			  objBusinessDetailDTO.setMsgContent(objResultSet.getString("sms_content"));
			  objBusinessDetailDTO.setSt(objResultSet.getTimestamp("date_time"));
			  objArrayList.add(objBusinessDetailDTO);
		 }
		
		
 } 
 catch (Exception e) {
	e.printStackTrace();
 } 
 finally {
	
	 DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);

 }
		

	
	
	
	
	return objArrayList;
	}

	public int updateSmsBusinessDetail(int business_id) 
	{

		Connection objConnection = null;
		PreparedStatement objPreparedStatement = null;
		int result=0;
		try {
			 objConnection = DBConfig.connectToMainDB();
			 String query1="update userinfo_SA set status=1 where business_id=?"; 
			 objPreparedStatement=objConnection.prepareStatement(query1);
			 objPreparedStatement.setInt(1, business_id);
			 result=objPreparedStatement.executeUpdate();
			
			
	        } 
	 catch (Exception e) 
	 {
		e.printStackTrace();
	 } 
	 finally
	 {
		
		 DBConfig.disconnect(objConnection, objPreparedStatement, null);

	 }
		return result;
		
	}
	
	

	public ArrayList<BusinessDetailDTO> getCallPlanLeads()
	{
	Connection objConnection = null;
	PreparedStatement objPreparedStatement = null;
	
	ResultSet objResultSet = null;
	
	ArrayList<BusinessDetailDTO> objArrayList=new ArrayList<BusinessDetailDTO>();
		
	try {
		 objConnection = DBConfig.connectToMainDB();
				 
		 String query1="SELECT business_detail_SA.business_view_count," +
		 		"business_detail_SA.business_mob1,business_detail_SA.business_ph1," +
		 		"business_master_SA.business_name,business_master_SA.business_city,business_master_SA.business_area,business_master_SA.keyword1 FROM business_master_SA inner join business_detail_SA on " +
		 		"business_master_SA.business_id=business_detail_SA.business_id where business_master_SA.business_priority<4 and business_detail_SA.business_view_count>20 " +
		 		"order by business_detail_SA.business_view_count desc limit 0,40";
		 			
		 objPreparedStatement = objConnection.prepareStatement(query1);
		
		 objResultSet = objPreparedStatement.executeQuery();
		  
		  while(objResultSet.next())
		 {	
			  BusinessDetailDTO objBusinessDetailDTO=new BusinessDetailDTO();	
			  objBusinessDetailDTO.setView_count(objResultSet.getInt("business_view_count"));
			  objBusinessDetailDTO.setMobile(objResultSet.getString("business_mob1"));
			  objBusinessDetailDTO.setPh1(objResultSet.getString("business_ph1"));
			  objBusinessDetailDTO.setBusiness_name(objResultSet.getString("business_name"));
			  objBusinessDetailDTO.setCity(objResultSet.getString("business_city"));
			  objBusinessDetailDTO.setArea(objResultSet.getString("business_area"));
			  objBusinessDetailDTO.setCat(objResultSet.getString("keyword1"));
			 
			  objArrayList.add(objBusinessDetailDTO);
		 }
		
		
 } 
 catch (Exception e) {
	e.printStackTrace();
 } 
 finally {
	
	 DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);

 }
		

	
	
	
	
	return objArrayList;
	}
	
	
	public ArrayList<BusinessDetailDTO> getArabicCallPlanLeads()
	{
	Connection objConnection = null;
	PreparedStatement objPreparedStatement = null;
	
	ResultSet objResultSet = null;
	
	ArrayList<BusinessDetailDTO> objArrayList=new ArrayList<BusinessDetailDTO>();
		
	try {
		 objConnection = DBConfig.connectArabicDb();
				 
		 String query1="SELECT business_detail.business_view_count," +
		 		"business_detail.business_mob1,business_detail.business_ph1," +
		 		"business_master.business_name,business_master.business_city,business_master.business_area,business_master.keyword1 FROM business_master inner join business_detail on " +
		 		"business_master.business_id=business_detail.business_id where business_master.business_priority<4 and business_detail.business_view_count>20 " +
		 		"order by business_detail.business_view_count desc limit 0,40";
		 			
		 objPreparedStatement = objConnection.prepareStatement(query1);
		
		 objResultSet = objPreparedStatement.executeQuery();
		  
		  while(objResultSet.next())
		 {	
			  BusinessDetailDTO objBusinessDetailDTO=new BusinessDetailDTO();	
			  objBusinessDetailDTO.setView_count(objResultSet.getInt("business_view_count"));
			  objBusinessDetailDTO.setMobile(objResultSet.getString("business_mob1"));
			  objBusinessDetailDTO.setPh1(objResultSet.getString("business_ph1"));
			  objBusinessDetailDTO.setBusiness_name(objResultSet.getString("business_name"));
			  objBusinessDetailDTO.setCity(objResultSet.getString("business_city"));
			  objBusinessDetailDTO.setArea(objResultSet.getString("business_area"));
			  objBusinessDetailDTO.setCat(objResultSet.getString("keyword1"));
			 
			  objArrayList.add(objBusinessDetailDTO);
		 }
		
		
 } 
 catch (Exception e) {
	e.printStackTrace();
 } 
 finally {
	
	 DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);

 }
		

	
	
	
	
	return objArrayList;
	}
	
	

}
