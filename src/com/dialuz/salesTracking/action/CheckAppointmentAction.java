
package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.opensymphony.xwork2.ActionSupport;

public class CheckAppointmentAction extends ActionSupport
{

	
	private static final long serialVersionUID = 2336730759173520794L;
	private AppointmentDTO objCheckAppointmentDTO=new AppointmentDTO();
	private String mailid;
	private String mobile;
	private String phoneno;
	private ArrayList<String> objarraylist;

	public ArrayList<String> getObjarraylist() {
		return objarraylist;
	}


	public void setObjarraylist(ArrayList<String> objarraylist) {
		this.objarraylist = objarraylist;
	}
	public String getMailid() {
		return mailid;
	}
	public void setMailid(String mailid) {
		this.mailid = mailid;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPhoneno() {
		return phoneno;
	}
	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}
	private ArrayList<AppointmentDTO> objAppointmentDTOList=new ArrayList<AppointmentDTO>();
	private ArrayList<AppointmentDTO> objAppointmentDTOList1=new ArrayList<AppointmentDTO>();
	public ArrayList<AppointmentDTO> getObjAppointmentDTOList() {
		return objAppointmentDTOList;
	}
	public void setObjAppointmentDTOList(ArrayList<AppointmentDTO> objAppointmentDTOList) {
		this.objAppointmentDTOList = objAppointmentDTOList;
	}
	
	public AppointmentDTO getObjCheckAppointmentDTO() {
		return objCheckAppointmentDTO;
	}
	public void setObjCheckAppointmentDTO(AppointmentDTO objCheckAppointmentDTO) {
		this.objCheckAppointmentDTO = objCheckAppointmentDTO;
	}
	public String execute()
	{
	
		objCheckAppointmentDTO.setBusinessMobile(mobile);
		objCheckAppointmentDTO.setBusinessPhone(phoneno);
		objCheckAppointmentDTO.setBusinessMail(mailid);
		
		AppointmentDAO objAppointmentDAO=new AppointmentDAO();
		
		objAppointmentDTOList=objAppointmentDAO.getAppointments(objCheckAppointmentDTO);
		objAppointmentDTOList1=objAppointmentDAO.getAppointments1(objCheckAppointmentDTO);
		
		//objarraylist=objAppointmentDAO.getCategoryDetails();
		
		if(objAppointmentDTOList.size()==0)
		{
			return SUCCESS;
		}else
		{
			return "appointment";
		}
		
		
	}


	public ArrayList<AppointmentDTO> getObjAppointmentDTOList1() {
		return objAppointmentDTOList1;
	}


	public void setObjAppointmentDTOList1(
			ArrayList<AppointmentDTO> objAppointmentDTOList1) {
		this.objAppointmentDTOList1 = objAppointmentDTOList1;
	}
	
}




















