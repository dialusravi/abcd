package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.dialuz.salesTracking.DAO.UsersOperationsDAO;
import com.dialuz.salesTracking.DTO.UsersOperationsDTO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class GetUsersAction extends ActionSupport
{

	
	private static final long serialVersionUID = 4833873819504530166L;
	private String type;
	private String userName;
	
	private ArrayList<String> objUsersList=new ArrayList<String>();
	private UsersOperationsDTO objUsersOperationsDTO=new UsersOperationsDTO();
	
	public UsersOperationsDTO getObjUsersOperationsDTO() {
		return objUsersOperationsDTO;
	}
	public void setObjUsersOperationsDTO(UsersOperationsDTO objUsersOperationsDTO) {
		this.objUsersOperationsDTO = objUsersOperationsDTO;
	}
	
	public ArrayList<String> getObjUsersList() {
		return objUsersList;
	}
	public void setObjUsersList(ArrayList<String> objUsersList) {
		this.objUsersList = objUsersList;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String execute()
	{
		HttpServletRequest request  = (HttpServletRequest)ActionContext.getContext().get(org.apache.struts2.StrutsStatics.HTTP_REQUEST);
		HttpSession session=request.getSession(true);
		
		String name=request.getParameter("q");
		
		UsersOperationsDAO objUsersOperationsDAO=new UsersOperationsDAO();
		objUsersOperationsDTO.setUserName(userName);
		objUsersOperationsDTO.setUsertype(type);
		objUsersOperationsDTO.setName(name);
		objUsersList=objUsersOperationsDAO.getAllUsers(objUsersOperationsDTO);
		return SUCCESS;
	}
	
}
