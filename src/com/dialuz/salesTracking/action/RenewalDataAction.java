package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.RenewalDataDAO;
import com.dialuz.salesTracking.DTO.RenewalDataDTO;
import com.opensymphony.xwork2.ActionSupport;

public class RenewalDataAction extends ActionSupport {

/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ArrayList<RenewalDataDTO> objArrayList=new ArrayList<RenewalDataDTO>();
	ArrayList<RenewalDataDTO> objArrayList1=new ArrayList<RenewalDataDTO>();
	

	public String execute(){
		
		RenewalDataDAO objRenewalDataDAO=new RenewalDataDAO();
		
		objArrayList=objRenewalDataDAO.getRenewalData();
		
		objArrayList1=objRenewalDataDAO.getArabicRenewalData();
		
		
		
		
		return "success";
	}

	public ArrayList<RenewalDataDTO> getObjArrayList() {
		return objArrayList;
	}

	public void setObjArrayList(ArrayList<RenewalDataDTO> objArrayList) {
		this.objArrayList = objArrayList;
	}

	public ArrayList<RenewalDataDTO> getObjArrayList1() {
		return objArrayList1;
	}

	public void setObjArrayList1(ArrayList<RenewalDataDTO> objArrayList1) {
		this.objArrayList1 = objArrayList1;
	}

	
}
