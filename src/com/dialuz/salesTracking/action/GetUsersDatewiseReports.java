package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.opensymphony.xwork2.ActionSupport;

public class GetUsersDatewiseReports extends ActionSupport
{

	
	private static final long serialVersionUID = 3904742690278259864L;
	private String type;
	private String userName;
	private String fromDate;
	private String toDate;
	private String resultPage;
	private ArrayList<AppointmentDTO> objAppointmentDTOList=new ArrayList<AppointmentDTO>();
	private ArrayList<AppointmentDTO> objAppointmentDTOList1=new ArrayList<AppointmentDTO>();
	private AppointmentDTO objAppointmentDTO=new AppointmentDTO();
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public ArrayList<AppointmentDTO> getObjAppointmentDTOList() {
		return objAppointmentDTOList;
	}
	public void setObjAppointmentDTOList(ArrayList<AppointmentDTO> objAppointmentDTOList) {
		this.objAppointmentDTOList = objAppointmentDTOList;
	}
	public AppointmentDTO getObjAppointmentDTO() {
		return objAppointmentDTO;
	}
	public void setObjAppointmentDTO(AppointmentDTO objAppointmentDTO) {
		this.objAppointmentDTO = objAppointmentDTO;
	}
	public String execute()
	{
		objAppointmentDTO.setAppointmentType(type);
		objAppointmentDTO.setFromDate(fromDate);
		objAppointmentDTO.setToDate(toDate);
		objAppointmentDTO.setUsername(userName);
		AppointmentDAO objAppointmentDAO=new AppointmentDAO();
		objAppointmentDTOList=objAppointmentDAO.getDatewiseReports(objAppointmentDTO);
		objAppointmentDTOList1=objAppointmentDAO.getArabicDatewiseReports(objAppointmentDTO);
		
		
		if(!type.equals("sales"))
		{
			
			resultPage="telecaller";
			
		}else
		{
			
			resultPage="sales";
		}
		return resultPage;
	}
	public ArrayList<AppointmentDTO> getObjAppointmentDTOList1() {
		return objAppointmentDTOList1;
	}
	public void setObjAppointmentDTOList1(
			ArrayList<AppointmentDTO> objAppointmentDTOList1) {
		this.objAppointmentDTOList1 = objAppointmentDTOList1;
	}
	
}
