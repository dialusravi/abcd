package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.opensymphony.xwork2.ActionSupport;

public class LoadAllAppointmentsAction extends ActionSupport 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8433426259253495917L;
	private ArrayList<AppointmentDTO> objCheckAppointmentDTOList=new ArrayList<AppointmentDTO>();
	private String type;
	public ArrayList<AppointmentDTO> getObjCheckAppointmentDTOList() {
		return objCheckAppointmentDTOList;
	}
	public void setObjCheckAppointmentDTOList(
			ArrayList<AppointmentDTO> objCheckAppointmentDTOList) {
		this.objCheckAppointmentDTOList = objCheckAppointmentDTOList;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String execute()
	{
		AppointmentDAO objAppointmentDAO=new AppointmentDAO();
		AppointmentDTO objAppointmentDTO1=new AppointmentDTO();
		objAppointmentDTO1.setAppointmentType(type);
		objCheckAppointmentDTOList=objAppointmentDAO.getAllRejectedAppointments(objAppointmentDTO1);
		return SUCCESS;
	}
	

}
