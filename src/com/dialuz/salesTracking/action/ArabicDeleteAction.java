package com.dialuz.salesTracking.action;

import com.dialuz.salesTracking.DAO.ArabicAutocompleteDao;
import com.opensymphony.xwork2.ActionSupport;

public class ArabicDeleteAction extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7545559633715472592L;
	private String delstr;
	private String city;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDelstr() {
		return delstr;
	}

	public void setDelstr(String delstr) {
		this.delstr = delstr;
	}
 public String execute()
 {
	 ArabicAutocompleteDao objAutoCompleteDao=new ArabicAutocompleteDao();
	 	  
	 int status=objAutoCompleteDao.arabicDeleteKeyword(delstr,city);
	 
	 return "success";
 }
}
