package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.BusinessDetailDao;
import com.dialuz.salesTracking.DTO.BusinessDetailDTO;
import com.opensymphony.xwork2.ActionSupport;

public class ArabicDailyCallPlanAction extends ActionSupport{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7357591036394198452L;
	ArrayList<BusinessDetailDTO> objArrayList=new ArrayList<BusinessDetailDTO>();

	public ArrayList<BusinessDetailDTO> getObjArrayList() {
		return objArrayList;
	}

	public void setObjArrayList(ArrayList<BusinessDetailDTO> objArrayList) {
		this.objArrayList = objArrayList;
	}
	
private String city;
public String execute(){
		
	
		BusinessDetailDao objBusinessDetailDao=new BusinessDetailDao();
		
		objArrayList=objBusinessDetailDao.getArabicDailyCallplan(city);
		
	
		return "success";
	}

public String getCity() {
	return city;
}

public void setCity(String city) {
	this.city = city;
}
	

}
