package com.dialuz.salesTracking.action;

import com.dialuz.salesTracking.DAO.AutoCompleteDao;
import com.opensymphony.xwork2.ActionSupport;

public class DeleteAction extends ActionSupport
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3360730599818836829L;
	private String delstr;
	private String city;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDelstr() {
		return delstr;
	}

	public void setDelstr(String delstr) {
		this.delstr = delstr;
	}
 public String execute()
 {
	 AutoCompleteDao objAutoCompleteDao=new AutoCompleteDao();
	 
	  //delstr=WordUtils.capitalize(delstr);
	 int status=objAutoCompleteDao.deleteKeyword(delstr,city);

	 	 return "success";
 }
}
