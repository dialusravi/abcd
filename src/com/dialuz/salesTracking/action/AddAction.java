package com.dialuz.salesTracking.action;

import java.util.ArrayList;


import org.apache.commons.lang.WordUtils;


import com.dialuz.salesTracking.DAO.AutoCompleteDao;
import com.opensymphony.xwork2.ActionSupport;

public class AddAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 40635550115449309L;
	private String country;
	private String city;
	private String successmessage;
	ArrayList<String> objArrayList=new ArrayList<String>();
	public ArrayList<String> getObjArrayList() {
		return objArrayList;
	}
	public void setObjArrayList(ArrayList<String> objArrayList) {
		this.objArrayList = objArrayList;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String execute()
	{
				
		AutoCompleteDao objAutoCompleteDao=new AutoCompleteDao();
		
		objArrayList=objAutoCompleteDao.getAllDetails();
			
		if(objArrayList.contains(country))
		{
			return "success";	
		}
		else 
		{
			country=WordUtils.capitalize(country);	
			
			
		String status=objAutoCompleteDao.insertCountry(country,city);
		
		
		
		}
		return "error";
		
	}
	public String getSuccessmessage() {
		return successmessage;
	}
	public void setSuccessmessage(String successmessage) {
		this.successmessage = successmessage;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}

}
