package com.dialuz.salesTracking.action;

import com.dialuz.salesTracking.DAO.TargetDao;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;

public class GetTargetAction extends ActionSupport
{

    public GetTargetAction()
    {
        objArrayList = new ArrayList();
    }

    public ArrayList getObjArrayList()
    {
        return objArrayList;
    }

    public void setObjArrayList(ArrayList objArrayList)
    {
        this.objArrayList = objArrayList;
    }

    public String execute()
    {
        TargetDao objTargetDao = new TargetDao();
        objArrayList = objTargetDao.getAllTargets(user);
        return "success";
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    private static final long serialVersionUID = 0x4771da2e196a6541L;
    private ArrayList objArrayList;
    private String user;
}