package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import com.dialuz.salesTracking.DAO.AutoCompleteDao;
import com.opensymphony.xwork2.ActionContext;

public class ArabicDialyCallPlanCityAutoComleteAction {
	public ArabicDialyCallPlanCityAutoComleteAction()
    {
        list = new ArrayList();
    }

    
    public ArrayList getList()
    {
        return list;
    }

    public void setList(ArrayList list)
    {
        this.list = list;
    }

    public String execute()
    {
        HttpServletRequest request = (HttpServletRequest)ActionContext.getContext().get("com.opensymphony.xwork2.dispatcher.HttpServletRequest");
        city = request.getParameter("q");
        AutoCompleteDao objAutocomplteDao = new AutoCompleteDao();
        list = objAutocomplteDao.getDialyCallPlanCityArabic(city);
        return "success";
    }

    private static final long serialVersionUID = 0x1f786053aea7c585L;
    private String city;
    public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}

	ArrayList list;
}
