package com.dialuz.salesTracking.action;


import java.io.FileInputStream;
import java.io.IOException;




import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



public class Download1 extends HttpServlet {
    /**
	 * 
	 */
	private static final long serialVersionUID = -714226106744383854L;

	public void doGet(HttpServletRequest request,HttpServletResponse response)
            throws ServletException, IOException
            {

    	    	
    	
	  String path =getServletContext().getInitParameter("filepath_pdf");
   // String path="C:\\Users\\venkateswararao.ch\\Desktop\\student.pdf";
		
        String filename = path;


        ServletOutputStream out = response.getOutputStream();
        FileInputStream in = new FileInputStream(filename);

        response.setContentType("application/pdf");
        response.addHeader("content-disposition","attachment; filename="+ filename);

        int octet;
        while((octet = in.read()) != -1)
            out.write(octet);

        in.close();
        out.close();
    }

    public void doPost(HttpServletRequest request,HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request,response);
    }
}