package com.dialuz.salesTracking.action;

import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import com.dialuz.salesTracking.DAO.SelectSubCategoryDAO;
import com.dialuz.salesTracking.DTO.SelectSubCategoryDTO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class SelectSubCategoryAction extends ActionSupport {
	private static final long serialVersionUID = 1L;


	ArrayList<String> objsubcategorylist= new ArrayList<String>();

	ArrayList<String> objsubsubcategorylist= new ArrayList<String>();

	ArrayList<String> objsubcategory= new ArrayList<String>();

	ArrayList<String> objsubsubcategory= new ArrayList<String>();


	public ArrayList<String> getObjsubcategory() {
		return objsubcategory;
	}
	public void setObjsubcategory(ArrayList<String> objsubcategory) {
		this.objsubcategory = objsubcategory;
	}
	public ArrayList<String> getObjsubsubcategory() {
		return objsubsubcategory;
	}
	public void setObjsubsubcategory(ArrayList<String> objsubsubcategory) {
		this.objsubsubcategory = objsubsubcategory;
	}
	public ArrayList<String> getObjsubcategorylist() {
		return objsubcategorylist;
	}
	public void setObjsubcategorylist(ArrayList<String> objsubcategorylist) {
		this.objsubcategorylist = objsubcategorylist;
	}
	public ArrayList<String> getObjsubsubcategorylist() {
		return objsubsubcategorylist;
	}
	public void setObjsubsubcategorylist(ArrayList<String> objsubsubcategorylist) {
		this.objsubsubcategorylist = objsubsubcategorylist;
	}
	private ArrayList<String> objArrayListSubCat=new ArrayList<String>();
	private ArrayList<String>  objArrayListSubSubCat=new ArrayList<String>();

	public ArrayList<String> getObjArrayListSubSubCat() {
		return objArrayListSubSubCat;
	}
	public void setObjArrayListSubSubCat(ArrayList<String> objArrayListSubSubCat) {
		this.objArrayListSubSubCat = objArrayListSubSubCat;
	}
	private String objStringSubCat;
	private String catName;

	public ArrayList<String> getObjArrayListSubCat() {
		return objArrayListSubCat;
	}
	public void setObjArrayListSubCat(ArrayList<String> objArrayListSubCat) {
		this.objArrayListSubCat = objArrayListSubCat;
	}
		public String getCatName() {
		return catName;
	}
	public void setCatName(String catName) {
		this.catName = catName;
	}
	public String execute() throws Exception {
					HttpServletRequest request  = (HttpServletRequest)
					ActionContext.getContext().get(org.apache.struts2.StrutsStatics.HTTP_REQUEST);
					 javax.servlet.http.HttpSession session = request.getSession();
					
					catName=request.getParameter("allcategoryid");
					
					
	 SelectSubCategoryDTO objSelectSubCategoryDTO=new SelectSubCategoryDTO();
	 SelectSubCategoryDAO objSelectSubCategoryDAO=new SelectSubCategoryDAO();

	 objSelectSubCategoryDTO.setCatName(catName);
	 
	 objArrayListSubCat=objSelectSubCategoryDAO.getSubCategory(catName);
	 
	 
	 
	 
	 SelectSubCategoryDTO objSelectSubCategoryDTO1=new SelectSubCategoryDTO();
	 
	 
	 
	 Iterator it= objArrayListSubCat.iterator();
		try{
		while(it.hasNext())
		{
			
			
			objSelectSubCategoryDTO1=(SelectSubCategoryDTO) it.next();
			
			
			objsubcategory.add(objSelectSubCategoryDTO1.getSubCategoryName());
			
			objsubsubcategory.add(objSelectSubCategoryDTO1.getSubSubCategoryName());
			
			
		}
		}catch (Exception e) {
		}
	 
	// subcategory
		 Iterator it1= objsubcategory.iterator();
		try{
		 while(it1.hasNext())
			{
				
			
				String cat= (String)it1.next();
				
				String cat1[]=cat.split(",");
				
				for(int i=0;i<cat1.length;i++)
				objsubcategorylist.add(cat1[i]);
				
	 
				
			}
		}catch (Exception e) {
		}
		 
		//sub subcategory
			 Iterator it2= objsubsubcategory.iterator();
			try{
			 while(it2.hasNext())
				{
					
				
					String cat= (String)it2.next();
					
					String cat1[]=cat.split(",");
					
					for(int i=0;i<cat1.length;i++)
						objsubsubcategorylist.add(cat1[i]);
					
		 
				}
			}catch (Exception e) {
			}
				 
			  return SUCCESS;

			  
		 
		
		
		}
		}

