package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.LoginDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.opensymphony.xwork2.ActionSupport;

public class ReportsAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<String> telecallerNames;
	private String param;
	public String getParam() {
		return param;
	}
	public void setParam(String param) {
		this.param = param;
	}
	public ArrayList<String> getTelecallerNames() {
		return telecallerNames;
	}
	public void setTelecallerNames(ArrayList<String> telecallerNames) {
		this.telecallerNames = telecallerNames;
	}
	private ArrayList<AppointmentDTO> salesPersons;
	public String execute()
	{
		
		
		LoginDAO objLoginDAO=new LoginDAO();
        if(param.equals("tele")){
		telecallerNames=objLoginDAO.getTelecallerNames();
		
        return SUCCESS;
		    }else {
		    	salesPersons=objLoginDAO.getSalesPersonNames();
		    	 return "success1";
			}
	}
	public ArrayList<AppointmentDTO> getSalesPersons() {
		return salesPersons;
	}
	public void setSalesPersons(ArrayList<AppointmentDTO> salesPersons) {
		this.salesPersons = salesPersons;
	}

}
