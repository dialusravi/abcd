package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.BusinessDetailDao;
import com.dialuz.salesTracking.DTO.BusinessDetailDTO;
import com.opensymphony.xwork2.ActionSupport;

public class CallPlanDataAction extends ActionSupport
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5530590262334030488L;
	ArrayList<BusinessDetailDTO> list=new ArrayList<BusinessDetailDTO>();
	ArrayList<BusinessDetailDTO> list1=new ArrayList<BusinessDetailDTO>();
	public ArrayList<BusinessDetailDTO> getList()
	{
		return list;
	}
	public void setList(ArrayList<BusinessDetailDTO> list)
	{
		this.list = list;
	}
	
	public String execute()
	{
		BusinessDetailDao objBusinessDetailDao=new BusinessDetailDao();
		list=objBusinessDetailDao.getCallPlanLeads();
		list1=objBusinessDetailDao.getArabicCallPlanLeads();
		
		return "success";
	}
	public ArrayList<BusinessDetailDTO> getList1() {
		return list1;
	}
	public void setList1(ArrayList<BusinessDetailDTO> list1) {
		this.list1 = list1;
	}

}
