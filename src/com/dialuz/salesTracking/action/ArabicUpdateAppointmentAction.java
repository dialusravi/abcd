package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.opensymphony.xwork2.ActionSupport;

public class ArabicUpdateAppointmentAction extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5549737460735700434L;
	private String appointmentId;
	private AppointmentDTO objCheckAppointmentDTO=new AppointmentDTO();
	private ArrayList<String> objarraylist;
	public ArrayList<String> getObjarraylist() {
		return objarraylist;
	}


	public void setObjarraylist(ArrayList<String> objarraylist) {
		this.objarraylist = objarraylist;
	}
	public String getAppointmentId() {
		return appointmentId;
	}
	public void setAppointmentId(String appointmentId) {
		this.appointmentId = appointmentId;
	}
	public AppointmentDTO getObjCheckAppointmentDTO() {
		return objCheckAppointmentDTO;
	}
	public void setObjCheckAppointmentDTO(AppointmentDTO objCheckAppointmentDTO) {
		this.objCheckAppointmentDTO = objCheckAppointmentDTO;
	}
	
	public String execute()
	{
		objCheckAppointmentDTO.setAppointmentId(appointmentId);
		objCheckAppointmentDTO.setUsertype("user");
		AppointmentDAO objAppointmentDAO=new AppointmentDAO();
		//objarraylist=objAppointmentDAO.getCategoryDetails();
		objCheckAppointmentDTO=objAppointmentDAO.getAppointmentDetails1(objCheckAppointmentDTO);
		return SUCCESS;
	}
}
