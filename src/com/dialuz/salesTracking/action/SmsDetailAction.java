package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.BusinessDetailDao;
import com.opensymphony.xwork2.ActionSupport;

public class SmsDetailAction extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3491380003377567150L;
	
	private ArrayList objArrayList=new ArrayList();

	public ArrayList getObjArrayList() {
		return objArrayList;
	}

	public void setObjArrayList(ArrayList objArrayList) {
		this.objArrayList = objArrayList;
	}
	public String execute(){
	BusinessDetailDao objBusinessDetailDao=new BusinessDetailDao();
	objArrayList=objBusinessDetailDao.getAllSms();
	
	return "success";
}
}