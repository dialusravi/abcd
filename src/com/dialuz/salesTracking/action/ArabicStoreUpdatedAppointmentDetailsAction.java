package com.dialuz.salesTracking.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class ArabicStoreUpdatedAppointmentDetailsAction extends ActionSupport{
	
	private AppointmentDTO objCheckAppointmentDTO=new AppointmentDTO();
	private String success_msg;
	
	
	
	public AppointmentDTO getObjCheckAppointmentDTO() {
		return objCheckAppointmentDTO;
	}
	public void setObjCheckAppointmentDTO(AppointmentDTO objCheckAppointmentDTO) {
		this.objCheckAppointmentDTO = objCheckAppointmentDTO;
	}
	public String execute()
	{
		HttpServletRequest request  = (HttpServletRequest)ActionContext.getContext().get(org.apache.struts2.StrutsStatics.HTTP_REQUEST);
		HttpSession session=request.getSession(true);
		objCheckAppointmentDTO.setTelecallerName((String)session.getAttribute("username")); 
		objCheckAppointmentDTO.setTelecallerMobile((String)session.getAttribute("usermobile")); 
		
		boolean insert_status=false;
		AppointmentDAO objAppointmentDAO=new AppointmentDAO();
		try
		{
			if(objCheckAppointmentDTO.getAppointmentTime().endsWith("M"))
			{
				String s[]=objCheckAppointmentDTO.getAppointmentTime().split(" ");
				objCheckAppointmentDTO.setAppointmentTime(s[0]+":00");
				
			}
		}catch(Exception e){e.printStackTrace();}
		
		
		insert_status=objAppointmentDAO.storeArabicAppointmentDetailsForUpadte(objCheckAppointmentDTO);
		if(insert_status)
		{
			success_msg="Your Appointment Details Has Been Submitted Successfully";
			return SUCCESS;
		}else
		{
			success_msg="Sorry Error Occured While Submitting Details....";
			return ERROR;
		}
		
	}
	public String getSuccess_msg() {
		return success_msg;
	}
	public void setSuccess_msg(String success_msg) {
		this.success_msg = success_msg;
	}
	
	

}
