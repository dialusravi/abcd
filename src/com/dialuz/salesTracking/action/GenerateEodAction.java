package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class GenerateEodAction extends ActionSupport
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1865420428590472362L;
	private String type;
	private AppointmentDTO objCheckAppointmentDTO=new AppointmentDTO();
	ArrayList<AppointmentDTO> objCheckAppointmentDTOList=new ArrayList<AppointmentDTO>();
	public String getType() {
		return type;
	}
	public void setType(String type) 
	{
		this.type = type;
	}
	public AppointmentDTO getObjCheckAppointmentDTO() {
		return objCheckAppointmentDTO;
	}
	public void setObjCheckAppointmentDTO(AppointmentDTO objCheckAppointmentDTO) {
		this.objCheckAppointmentDTO = objCheckAppointmentDTO;
	}
	public ArrayList<AppointmentDTO> getObjCheckAppointmentDTOList() {
		return objCheckAppointmentDTOList;
	}
	public void setObjCheckAppointmentDTOList(
			ArrayList<AppointmentDTO> objCheckAppointmentDTOList) {
		this.objCheckAppointmentDTOList = objCheckAppointmentDTOList;
	}

	public String execute()
	{
		AppointmentDAO objAppointmentDAO=new AppointmentDAO();
		HttpServletRequest request  = (HttpServletRequest)ActionContext.getContext().get(org.apache.struts2.StrutsStatics.HTTP_REQUEST);
		HttpSession session=request.getSession(true);
		objCheckAppointmentDTO.setTelecallerName((String)session.getAttribute("username"));
		objCheckAppointmentDTO.setAppointmentType(type);
		objCheckAppointmentDTOList=objAppointmentDAO.getDailyEodDetails(objCheckAppointmentDTO);
		if(type.equals("daily"))
		{
			return "daily";
		}else if(type.equals("weekly"))
		{
			return "weekly";
		}else
		{
			return "monthly";
		}
	}
	
	
}
