package com.dialuz.salesTracking.action;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts2.ServletActionContext;


import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class ExportToExcelAction extends ActionSupport
{
	private static final long serialVersionUID = -1281231945981661100L;
	private AppointmentDTO objCheckAppointmentDTO=new AppointmentDTO();
	private ArrayList<String> objSalesPersonsList=new ArrayList<String>();
	private String type;
	private String usertype;
	private String appointmentId;
	private String appointmentPlace;
	private String success_msg;
	private String appointmentCity;
	private String tellcallername;
	private String todaydate;
	private String append_todaydate;
	private String param;
	private String param1;
	
	public String getAppend_todaydate() {
		return append_todaydate;
	}
	public void setAppend_todaydate(String append_todaydate) {
		this.append_todaydate = append_todaydate;
	}
	public String getTodaydate() {
		return todaydate;
	}
	public void setTodaydate(String todaydate) {
		this.todaydate = todaydate;
	}
	public String getTellcallername() {
		return tellcallername;
	}
	public void setTellcallername(String tellcallername) {
		this.tellcallername = tellcallername;
	}
	String resultpage;
	public String getAppointmentId() {
		return appointmentId;
	}
	public String getAppointmentPlace() {
		return appointmentPlace;
	}
	public void setAppointmentPlace(String appointmentPlace) {
		this.appointmentPlace = appointmentPlace;
	}
	public void setAppointmentId(String appointmentId) {
		this.appointmentId = appointmentId;
	}
	private ArrayList<AppointmentDTO> objAppointmentDTOList=new ArrayList<AppointmentDTO>();
	private ArrayList<AppointmentDTO> objAppointmentDTOList1=new ArrayList<AppointmentDTO>();
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public AppointmentDTO getObjCheckAppointmentDTO() {
		return objCheckAppointmentDTO;
	}
	public void setObjCheckAppointmentDTO(AppointmentDTO objCheckAppointmentDTO) {
		this.objCheckAppointmentDTO = objCheckAppointmentDTO;
	}
	public ArrayList<AppointmentDTO> getObjAppointmentDTOList()
	{
		return objAppointmentDTOList;
	}
	public void setObjAppointmentDTOList(ArrayList<AppointmentDTO> objAppointmentDTOList) {
		this.objAppointmentDTOList = objAppointmentDTOList;
	}
	public String getAppointmentCity() {
		return appointmentCity;
	}
	public void setAppointmentCity(String appointmentCity) {
		this.appointmentCity = appointmentCity;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public ArrayList<String> getObjSalesPersonsList() {
		return objSalesPersonsList;
	}
	public void setObjSalesPersonsList(ArrayList<String> objSalesPersonsList) {
		this.objSalesPersonsList = objSalesPersonsList;
	}
	
	@SuppressWarnings("deprecation")
	public String execute()
	{
			
		
		
		
		
		HttpServletRequest request  = (HttpServletRequest)ActionContext.getContext().get(org.apache.struts2.StrutsStatics.HTTP_REQUEST);
		HttpSession session=request.getSession(true);
		AppointmentDAO objAppointmentDAO=new AppointmentDAO();
		objCheckAppointmentDTO.setUsertype(usertype);
		objCheckAppointmentDTO.setTodaydate(todaydate);
		objCheckAppointmentDTO.setAppointmentCity(appointmentCity);
		objCheckAppointmentDTO.setAppointmentId(appointmentId);
		objCheckAppointmentDTO.setAppointmentPlace(appointmentPlace);
		objCheckAppointmentDTO.setTelecallerName(tellcallername);
		session.setAttribute("todaydate", todaydate);
		//ExcelCreate objExcelCreate=new ExcelCreate();	
		usertype=(String)session.getAttribute("usertype");
	   	      
        
        
    	//  if(param==null || param=="")
    	 // {
    		
    			success_msg="All Results Exported Successfully";
    			
    		 objAppointmentDTOList=objAppointmentDAO.getAllAppointments(objCheckAppointmentDTO);
    		 objAppointmentDTOList1=objAppointmentDAO.getAllAppointments1(objCheckAppointmentDTO);
    		
    		  
    		  try{
    			  			
    		 ServletContext context = ServletActionContext.getServletContext();
    		 String path = context.getInitParameter("filepath_excelsheet");
    			
    			
    			String filename=path ;
    			
    			HSSFWorkbook hwb=new HSSFWorkbook();
    			HSSFSheet sheet =  hwb.createSheet("new sheet");

    			HSSFRow rowhead=   sheet.createRow((short)0);
    			rowhead.createCell((short) 0).setCellValue("AppointMentId");
    			rowhead.createCell((short) 1).setCellValue("BusinessName");
    			rowhead.createCell((short) 2).setCellValue("AppointmentDateTime");
    			rowhead.createCell((short) 3).setCellValue("BusinessCity");
    			rowhead.createCell((short) 4).setCellValue("TelecallerName");
    			rowhead.createCell((short) 5).setCellValue("BusinessArea");
    			
    			
    			int i=1;
    			
    			Iterator itr = objAppointmentDTOList.iterator();
    			
    		      while(itr.hasNext())
    		      {
    		    	  
    		      AppointmentDTO objArabicFormDTO = (AppointmentDTO)itr.next();
    		    	 
    		       HSSFRow row=   sheet.createRow((short)i);
    		    	 
    		    	row.createCell((short) 0).setCellValue((objArabicFormDTO.getAppointmentId()));
    		  		row.createCell((short) 1).setCellValue(objArabicFormDTO.getBusinessName());
    		  		row.createCell((short) 2).setCellValue(objArabicFormDTO.getAppointmentDateTime());
    		  		row.createCell((short) 3).setCellValue(objArabicFormDTO.getBusinessCity());
    				row.createCell((short) 4).setCellValue(objArabicFormDTO.getTelecallerName());
    				row.createCell((short) 5).setCellValue(objArabicFormDTO.getBusinessArea());
    				
    				
    				
    				
    				
    				
    				
    		        i++;
    		      }
    		    		   
        			
        		
    	         
    		FileOutputStream fileOut =  new FileOutputStream(filename);
    		hwb.write(fileOut);
    		
    		Iterator itr1 = objAppointmentDTOList1.iterator();
			
		      while(itr1.hasNext())
		      {
		    	  
		      AppointmentDTO objArabicFormDTO1 = (AppointmentDTO)itr1.next();
		    	 
		       HSSFRow row=   sheet.createRow((short)i);
		    	 
		    	row.createCell((short) 0).setCellValue((objArabicFormDTO1.getAppointmentId()));
		  		row.createCell((short) 1).setCellValue(objArabicFormDTO1.getArabicBusinessName());
		  		row.createCell((short) 2).setCellValue(objArabicFormDTO1.getAppointmentDateTime());
		  		row.createCell((short) 3).setCellValue(objArabicFormDTO1.getArabicBusinessCity());
				row.createCell((short) 4).setCellValue(objArabicFormDTO1.getTelecallerName());
				row.createCell((short) 5).setCellValue(objArabicFormDTO1.getArabicBusinessArea());
				   		        
		        i++;
		      }
		      
		      FileOutputStream fileOut1 =  new FileOutputStream(filename);
	    		
		      hwb.write(fileOut1);
    		
    		
    		
    		
    		
    		
    		fileOut1.close();
    		
	}catch (Exception e)
	{
	e.printStackTrace();
		
	}
    		  
  
     	   
     	   return "success1";
    	  
    	
      
     
      
    	 
     	
		
	}
	public String getSuccess_msg() {
		return success_msg;
	}
	public void setSuccess_msg(String success_msg) {
		this.success_msg = success_msg;
	}
	public String getParam() {
		return param;
	}
	public void setParam(String param) {
		this.param = param;
	}
	public ArrayList<AppointmentDTO> getObjAppointmentDTOList1() {
		return objAppointmentDTOList1;
	}
	public void setObjAppointmentDTOList1(ArrayList<AppointmentDTO> objAppointmentDTOList1) {
		this.objAppointmentDTOList1 = objAppointmentDTOList1;
	}
	public String getParam1() {
		return param1;
	}
	public void setParam1(String param1) {
		this.param1 = param1;
	}
	
	
}
