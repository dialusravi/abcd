package com.dialuz.salesTracking.action;

import com.dialuz.salesTracking.DAO.TargetDao;
import com.opensymphony.xwork2.ActionSupport;

public class TargetAction extends ActionSupport
{

    public TargetAction()
    {
    }

    public String getStatus()
    {
        return status143;
    }

    public void setStatus(String status143)
    {
        this.status143 = status143;
    }

    public String getTargetuser()
    {
        return targetuser;
    }

    public void setTargetuser(String targetuser)
    {
        this.targetuser = targetuser;
    }

    public int getTargets()
    {
        return targets;
    }

    public void setTargets(int targets)
    {
        this.targets = targets;
    }

    public String execute()
    {
        TargetDao objTargetDao = new TargetDao();
        boolean count = objTargetDao.insertTargets(targetuser, targets, tlname);
        if(count)
            status143 = "Successfully Inserted";
        else
            status143 = "Successfully Not Inserted";
        return "success";
    }

    public String getTlname()
    {
        return tlname;
    }

    public void setTlname(String tlname)
    {
        this.tlname = tlname;
    }

    private static final long serialVersionUID = 0xa78fcadbab693190L;
    private String targetuser;
    private int targets;
    private String tlname;
    private String status143;
}