package com.dialuz.salesTracking.action;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.servlet.ServletContext;

import org.apache.struts2.ServletActionContext;

import com.dialuz.salesTracking.DAO.BusinessDetailDao;
import com.dialuz.salesTracking.DTO.BusinessDetailDTO;
import com.dialuz.salesTracking.commonUtilities.SendMail;
import com.dialuz.salesTracking.commonUtilities.SendSms;
import com.opensymphony.xwork2.ActionSupport;

public class SendingBusinessDetailAction extends ActionSupport {

	/**
	 * 
	 */
	  private String  mobile_no;
	  private String  email_id;
	  private String  msg_content;
	  private String cat;
	  private String name;
	  private String business_name;
	  private int business_id;
	  private String business_city;
	  ArrayList obArrayList;
		
		public ArrayList getObArrayList()
		{
			return obArrayList;
		}

		public void setObArrayList(ArrayList obArrayList)
		{
			this.obArrayList = obArrayList;
		}
	public int getBusiness_id() {
		return business_id;
	}

	public void setBusiness_id(int business_id) {
		this.business_id = business_id;
	}

	ArrayList business_priority_list=new ArrayList();
	ArrayList business_priority=new ArrayList();
	  
	public ArrayList getBusiness_priority() {
		return business_priority;
	}

	public void setBusiness_priority(ArrayList business_priority) {
		this.business_priority = business_priority;
	}

	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMsg_content() {
		return msg_content;
	}

	public void setMsg_content(String msg_content) {
		this.msg_content = msg_content;
	}

	public String getEmail_id() {
		return email_id;
	}

	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}

	public String getMobile_no() {
		return mobile_no;
	}

	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}

	private static final long serialVersionUID = 2167672502544132061L;

	public String execute()
	{
		
		
		
		
		
		msg_content="You have a lead from dialus.com. "+name+" has searched for "+cat+" .His/Her Contact information is"+mobile_no+","+email_id+" Grab this business now.";
				
		ServletContext context = ServletActionContext.getServletContext();
		
		BusinessDetailDao objBusinessDetailDao=new BusinessDetailDao();
		SendSms objSendSms=new SendSms();
		BusinessDetailDTO objBusinessDetailDTO=new BusinessDetailDTO();
	    
		/*business_priority=objBusinessDetailDao.getPriority(cat);
		
		
		ArrayList<Integer> list1=new ArrayList<Integer>();
		ArrayList<Integer> list2=new ArrayList<Integer>();
		ArrayList<BusinessDetailDTO> list123=new ArrayList<BusinessDetailDTO>();
		
		Iterator<BusinessDetailDTO> ite=business_priority.iterator();*/
		
		 int status=objBusinessDetailDao.updateSmsBusinessDetail(business_id);
		
		
		/*while(ite.hasNext())
		{
			objBusinessDetailDTO=(BusinessDetailDTO)ite.next();
			
			
			if(objBusinessDetailDTO.getPriority()>=7)
			{
				list1.add(objBusinessDetailDTO.getPriority());
				
			}else{
				list2.add(objBusinessDetailDTO.getPriority());
			    }
						
		}*/
		
		
		//if(list1.size()!=0)
		//{
					
			business_priority_list=objBusinessDetailDao.getBusinessPriority(cat,business_city);
						
			Iterator e1=business_priority_list.iterator();
			
	    while(e1.hasNext())
		    {
		    	objBusinessDetailDTO=(BusinessDetailDTO)e1.next();   
		    		
		    	if(objBusinessDetailDTO.getMobile()!=null)
		    	{
		    		
		    		
		    			    		    		
		       objSendSms.sendSms(msg_content,objBusinessDetailDTO.getMobile());
		        objBusinessDetailDao.insertSmsContent(objBusinessDetailDTO.getBusiness_name(),objBusinessDetailDTO.getMobile(),business_id,msg_content);
		    	 status=objBusinessDetailDao.updateSmsBusinessDetail(business_id);
		    	
		    	 if(status>0)
				{						
					obArrayList=objBusinessDetailDao.getBusinessDetail();
								    
				}
		    	
		    	}
		    	
		    	if(objBusinessDetailDTO.getEmail()!=null)
		    	{
		    		try{
		    			String BusinessDetailOne=context.getInitParameter("mailparametersforbusinessowner");
		    			File fileBusinessDetOne=new File(BusinessDetailOne);
		    			BufferedReader brBusinessData1 = new BufferedReader(new FileReader(fileBusinessDetOne));
		    			 StringBuilder sbBusinesDetOne = new StringBuilder();
		    			 String lineBusinessOne = brBusinessData1.readLine();
		    			 while (lineBusinessOne != null) 
		    			 {
		    				 sbBusinesDetOne.append(lineBusinessOne);
		    				 sbBusinesDetOne.append('\n');
		    		        	lineBusinessOne = brBusinessData1.readLine();
		    		     }
		    			 String BusinessMailOne = sbBusinesDetOne.toString();
		    			 
		    			 BusinessMailOne= BusinessMailOne.replace("--userName--",name);
		    			 BusinessMailOne= BusinessMailOne.replace("--userSerch--",cat);
		    			 BusinessMailOne= BusinessMailOne.replace("--userInfo--",mobile_no+","+email_id);
		    			
		    			 
		    			String maildetails=BusinessMailOne.toString();
		    		
		    		
		    			
		    			
		    		SendMail.sendMail(objBusinessDetailDTO.getEmail(), maildetails);
		    			
		    		}
		    		catch (Exception e3) {
		    			
		    		}	
		    	  }
		    	}
	   	    
	    
		    	
		//}  	
	/*else
	{
				

			business_priority_list=objBusinessDetailDao.getBusinessPriority4(cat,business_city);
					
			Iterator e12=business_priority_list.iterator();
			
		  while(e12.hasNext())
		    {
		    	objBusinessDetailDTO=(BusinessDetailDTO)e12.next();   
		    		
		    	if(objBusinessDetailDTO.getMobile()!=null)
		    	{
		    			
		       objSendSms.sendSms(msg_content,objBusinessDetailDTO.getMobile());
		        objBusinessDetailDao.insertSmsContent(objBusinessDetailDTO.getBusiness_name(),objBusinessDetailDTO.getMobile(),business_id,msg_content);
		     status=objBusinessDetailDao.updateSmsBusinessDetail(business_id);
		    	
		        if(status>0)
				{			
					
				obArrayList=objBusinessDetailDao.getBusinessDetail();
												    
				}
		    	
		    	}
		    	if(objBusinessDetailDTO.getEmail()!=null)
		    	{
		    		try{
		    			String BusinessDetailOne=context.getInitParameter("mailparametersforbusinessowner");
		    			File fileBusinessDetOne=new File(BusinessDetailOne);
		    			BufferedReader brBusinessData1 = new BufferedReader(new FileReader(fileBusinessDetOne));
		    			 StringBuilder sbBusinesDetOne = new StringBuilder();
		    			 String lineBusinessOne = brBusinessData1.readLine();
		    			 while (lineBusinessOne != null) {
		    				 sbBusinesDetOne.append(lineBusinessOne);
		    				 sbBusinesDetOne.append('\n');
		    		        	lineBusinessOne = brBusinessData1.readLine();
		    		        }
		    			 String BusinessMailOne = sbBusinesDetOne.toString();
		    			 
		    			 BusinessMailOne= BusinessMailOne.replace("--userName--",name);
		    			 BusinessMailOne= BusinessMailOne.replace("--userSerch--",cat);
		    			 BusinessMailOne= BusinessMailOne.replace("--userInfo--",mobile_no+","+email_id);
		    			
		    			 
		    			String maildetails=BusinessMailOne.toString();
		    		//SendMail.sendMail(objBusinessDetailDTO.getEmail(), maildetails);
		    			
		    		}
		    		catch (Exception e3) {
		    			
		    		}	
		    	  }
		  }
		  		
			
		}*/
		
		
		
		obArrayList=objBusinessDetailDao.getBusinessDetail();
	
	/*			
	Iterator e=business_priority.iterator();
	
	
	while(e.hasNext())
		{

		
	   objBusinessDetailDTO=(BusinessDetailDTO)e.next();  
		    
						  
			  if(objBusinessDetailDTO.getPriority()>=7)
		      {
		business_priority_list=objBusinessDetailDao.getBusinessPriority(cat);
		
		
		Iterator e1=business_priority_list.iterator();
		
    if(e1.hasNext())
	    {
	    	objBusinessDetailDTO=(BusinessDetailDTO)e1.next();   
	    		    	
	    	if(objBusinessDetailDTO.getMobile()!=null)
	    	{
	    	
	    		    		
	        objSendSms.sendSms(msg_content,objBusinessDetailDTO.getMobile());
	        objBusinessDetailDao.insertSmsContent(business_name,objBusinessDetailDTO.getMobile(),business_id,msg_content);
	    	}
	    	if(objBusinessDetailDTO.getEmail()!=null)
	    	{
	    		try{
	    			String BusinessDetailOne=context.getInitParameter("mailparametersforbusinessowner");
	    			File fileBusinessDetOne=new File(BusinessDetailOne);
	    			BufferedReader brBusinessData1 = new BufferedReader(new FileReader(fileBusinessDetOne));
	    			 StringBuilder sbBusinesDetOne = new StringBuilder();
	    			 String lineBusinessOne = brBusinessData1.readLine();
	    			 while (lineBusinessOne != null) 
	    			 {
	    				 sbBusinesDetOne.append(lineBusinessOne);
	    				 sbBusinesDetOne.append('\n');
	    		        	lineBusinessOne = brBusinessData1.readLine();
	    		     }
	    			 String BusinessMailOne = sbBusinesDetOne.toString();
	    			 
	    			 BusinessMailOne= BusinessMailOne.replace("--userName--",name);
	    			 BusinessMailOne= BusinessMailOne.replace("--userSerch--",cat);
	    			 BusinessMailOne= BusinessMailOne.replace("--userInfo--",mobile_no+","+email_id);
	    			
	    			 
	    			String maildetails=BusinessMailOne.toString();
	    		SendMail.sendMail(objBusinessDetailDTO.getEmail(), maildetails);
	    			
	    		}
	    		catch (Exception e3) {
	    			
	    		}	
	    	  }
	    	}
	    	
	    	
	    }*/
			  
		/*else if(objBusinessDetailDTO.getPriority()>=4 && objBusinessDetailDTO.getPriority()<7)
	    {
			business_priority_list=objBusinessDetailDao.getBusinessPriority4(cat);
			
			
			System.out.println("else if size"+business_priority_list.size());
			
			Iterator e1=business_priority_list.iterator();
			
		   if(e1.hasNext())
		    {
		    	objBusinessDetailDTO=(BusinessDetailDTO)e1.next();   
		    		
		    	if(objBusinessDetailDTO.getMobile()!=null)
		    	{
		    		System.out.println(">>>>>>>>in sms 2");	
		        //objSendSms.sendSms(msg_content,objBusinessDetailDTO.getMobile());
		        objBusinessDetailDao.insertSmsContent(business_name,objBusinessDetailDTO.getMobile(),business_id,msg_content);
		    	}
		    	if(objBusinessDetailDTO.getEmail()!=null)
		    	{
		    		try{
		    			String BusinessDetailOne=context.getInitParameter("mailparametersforbusinessowner");
		    			File fileBusinessDetOne=new File(BusinessDetailOne);
		    			BufferedReader brBusinessData1 = new BufferedReader(new FileReader(fileBusinessDetOne));
		    			 StringBuilder sbBusinesDetOne = new StringBuilder();
		    			 String lineBusinessOne = brBusinessData1.readLine();
		    			 while (lineBusinessOne != null) {
		    				 sbBusinesDetOne.append(lineBusinessOne);
		    				 sbBusinesDetOne.append('\n');
		    		        	lineBusinessOne = brBusinessData1.readLine();
		    		        }
		    			 String BusinessMailOne = sbBusinesDetOne.toString();
		    			 
		    			 BusinessMailOne= BusinessMailOne.replace("--userName--",name);
		    			 BusinessMailOne= BusinessMailOne.replace("--userSerch--",cat);
		    			 BusinessMailOne= BusinessMailOne.replace("--userInfo--",mobile_no+","+email_id);
		    			
		    			 
		    			String maildetails=BusinessMailOne.toString();
		    		//	SendMail.sendMail(objBusinessDetailDTO.getEmail(), maildetails);
		    			
		    		}
		    		catch (Exception e3) {
		    			
		    		}	
		    	  }
		    	}
		
		   
	    	
	    }*/
		
						
	//}
	
		
		
		
   return "success";    
}
	public String getBusiness_name() {
		return business_name;
	}

	public void setBusiness_name(String business_name) {
		this.business_name = business_name;
	}

	public String getBusiness_city() {
		return business_city;
	}

	public void setBusiness_city(String business_city) {
		this.business_city = business_city;
	}

	
	
	
	
	
}
