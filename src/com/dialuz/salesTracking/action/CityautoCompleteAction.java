package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.CityAutoCompleteSearchDAO;
import com.opensymphony.xwork2.ActionSupport;

public class CityautoCompleteAction extends ActionSupport{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8750367735448186871L;
	private String q;
	private String variable;
	private String businessState;
	private String businesCity;
	



	public String getBusinesCity() {
		return businesCity;
	}

	public void setBusinesCity(String businesCity) {
		this.businesCity = businesCity;
	}

	public String getBusinessState() {
		return businessState;
	}

	public void setBusinessState(String businessState) {
		this.businessState = businessState;
	}

	public String getVariable() {
		return variable;
	}

	public void setVariable(String variable) {
		this.variable = variable;
	}

	private ArrayList<String> CitySearchList;
	private ArrayList<String> StateSearchList;
	private ArrayList<String> AreaSearchList;

	public ArrayList<String> getAreaSearchList() {
		return AreaSearchList;
	}

	public void setAreaSearchList(ArrayList<String> areaSearchList) {
		AreaSearchList = areaSearchList;
	}

	public ArrayList<String> getStateSearchList() {
		return StateSearchList;
	}

	public void setStateSearchList(ArrayList<String> stateSearchList) {
		StateSearchList = stateSearchList;
	}

	public ArrayList<String> getCitySearchList() {
		return CitySearchList;
	}

	public void setCitySearchList(ArrayList<String> citySearchList) {
		CitySearchList = citySearchList;
	}

	public String getQ() {
		return q;
	}

	public void setQ(String q) {
		this.q = q;
	}
	
	
	public String execute() 
	{
		

		CityAutoCompleteSearchDAO objClassifiesHeaderSearchDAO=new CityAutoCompleteSearchDAO();
		
		try{
				if(variable.equals("city"))
				{
					CitySearchList=objClassifiesHeaderSearchDAO.getautoCompleteCitiesList(q,businessState);
				}
				else if(variable.equals("state")){
					
					StateSearchList=objClassifiesHeaderSearchDAO.getautoCompleteStateList(q);
				}
				
				else if(variable.equals("area")){
					
					AreaSearchList=objClassifiesHeaderSearchDAO.getautoCompleteAreaList(q,businesCity);
				}
				
		}
		catch (Exception e) 
		{
			
		}
		
		return SUCCESS;
	}
	
}