<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="css/css.css" rel="stylesheet" type="text/css" media="all" />

</head>
<body>
<div class="load_appointments">
	
	<div class="load_appointments_headding">
    <img src="images/appointment_missed.png" width="30" height="30" style="float:left; margin-left:15px;"> 
    <span style="float:left; margin-left:10px; margin-top:4px;"> Appointments Details</span>
    </div>
    
    <div class="load_appointments_arabicner">
    <div class="load_appointments_arabicner101" style="width:410px;">
    <s:hidden name="usertype" value="user"></s:hidden>
	<s:hidden name="objCheckAppointmentDTO.appointmentId" value="%{objCheckAppointmentDTO.appointmentId}"></s:hidden>
					      
    	<div class="load_appointments_arabicnername" style="width:400px;">
      Meet: <s:property value="%{objCheckAppointmentDTO.contactPerson}"/>
        </div>
        <div class="load_appointments_arabicnerplece" style="width:400px;">
        Near : <s:property value="%{objCheckAppointmentDTO.businessArea}"/>
        </div>
        <div class="load_appointments_arabicnertime" style="width:400px;">
       Contact Number : <s:property value="%{objCheckAppointmentDTO.contactNumber}"/> 
        </div>
           <div class="load_appointments_arabicnertime" style="width:400px;">
       Contact Person : <s:property value="%{objCheckAppointmentDTO.contactPerson}"/> 
        </div>
      </div>
        <div class="load_appointments_arabicner122" style="width:180px; margin-top:10px; color:#ff6600">
        
        <p style="color:#f30909;">On:<s:property value="%{objCheckAppointmentDTO.appointmentDate}"/></p><br/>
        <p style="color:#f30909;">At:<s:property value="%{objCheckAppointmentDTO.appointmentTime}"/></p>
      </div>
        
  </div>
    <div class="load_appointments_arabicner" style="background-color:#f0fafd">
    <div class="load_appointments_arabicner101" >
    	<div class="load_appointments_arabicnername">
       		<div class="load_appointments_arabicnername_left">
           Business Name :
            </div>
            <div class="load_appointments_arabicnername_right">
            <s:property value="%{objCheckAppointmentDTO.businessName}"/>
            </div>
        </div>
       
        <div class="load_appointments_arabicnername">
       		<div class="load_appointments_arabicnername_left">
            Business Category :
            </div>
            <div class="load_appointments_arabicnername_right">
            <s:property value="%{objCheckAppointmentDTO.businessCategory}"/>
            </div>
        </div>
         <div class="load_appointments_arabicnername">
       		<div class="load_appointments_arabicnername_left">
            Business Other Category :
            </div>
            <div class="load_appointments_arabicnername_right">
            <s:property value="%{objCheckAppointmentDTO.businessOtherCategory}"/>
            </div>
        </div>
         <div class="load_appointments_arabicnername">
       		<div class="load_appointments_arabicnername_left">
             Business Area :
            </div>
            <div class="load_appointments_arabicnername_right">
            <s:property value="%{objCheckAppointmentDTO.businessArea}"/>
            </div>
        </div>
        <div class="load_appointments_arabicnername">
       		<div class="load_appointments_arabicnername_left">
            Business Email :
            </div>
            <div class="load_appointments_arabicnername_right">
             <s:property value="%{objCheckAppointmentDTO.businessMail}"/>
            </div>
        </div>
        <div class="load_appointments_arabicnername">
       		<div class="load_appointments_arabicnername_left">
            Business Mobile :
            </div>
            <div class="load_appointments_arabicnername_right">
           <s:property value="%{objCheckAppointmentDTO.businessMobile}"/>
            </div>
        </div>
        <div class="load_appointments_arabicnername">
       		<div class="load_appointments_arabicnername_left">
             Business Phone :
            </div>
            <div class="load_appointments_arabicnername_right">
           <s:property value="%{objCheckAppointmentDTO.businessPhone}"/>
            </div>
        </div>
        <div class="load_appointments_arabicnername">
       		<div class="load_appointments_arabicnername_left">
              Business Phone1 :
            </div>
            <div class="load_appointments_arabicnername_right">
           <s:property value="%{objCheckAppointmentDTO.businessPhone1}"/>
            </div>
        </div>
        
      </div>
        
        
  </div>
  
  <div class="load_appointments_arabicner">
    <div class="load_appointments_arabicner101" >
      <div class="load_appointments_arabicnername">
      
      <input name="" type="button" value="Pending" onclick="pending('<s:property value="%{objCheckAppointmentDTO.appointmentId}" />')" style="margin-left:130px;"> <input name="" value="Completed" onclick="completed('<s:property value="%{objCheckAppointmentDTO.appointmentId}" />')"  type="button"> <input name="" value="Rejected" type="button" onclick="reject('<s:property value="%{objCheckAppointmentDTO.appointmentId}" />')">
      </div>
        
    </div>
        
        
  </div>
    <div style="clear:both"></div>
</div>


</body>
</html>