<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="css/css.css" rel="stylesheet" type="text/css" media="all" />

</head>
<body>
<div class="userappointments_line"></div>
<div class="userappointments_arabicner">
<img src="images/appointment_missed.png" width="30" height="30" style="float:left; margin-left:25px;"><span style="float:left; margin-top:3px; margin-left:10px;">SalesPersons Details</span> </div>
<div class="userappointments_line"></div>
<div class="userappointments_line" style="margin-top:10px;"></div>

<div class="userappointments">
	<div class="userbusinessname">
    Business Dev Officer Name
    </div>
    <div class="user_appointmentplace">
    Contact Number
    </div>
    <div class="user_appointmenttime">
     Business Dev Officer  City
    </div>
    <div class="user_appointmentfixedby">
    Business Dev Officer Area
    </div>
</div>

<div class="userappointments_line"></div>
<s:iterator value="objUsersOperationsDTOList" id="a">
<div class="userappointments_arabicner1">
<div class="userbusinessname">
    <s:property value="#a.userName"/>

  </div>
    <div class="user_appointmentplace">
    <s:property value="#a.userMobile"/>
    </div>
    <div class="user_appointmenttime">
    <s:property value="#a.userCity"/>
    </div>
    <div class="user_appointmentfixedby">
    <s:if test="%{#a.userArea!=null && #a.userArea!=''}">
    <s:property value="#a.userArea"/>
    </s:if>
    <s:else>
    ........
    </s:else>
    </div>
    <div class="user_appointmentalert">
    <a href="deleteUsers.action?type=salesperson&userId=<s:property value="#a.userId"/>"><input type="button"  value="Delete"/></a>

    </div>

</div>

</s:iterator>
</body>
</html>