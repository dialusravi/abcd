<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="css/css.css" rel="stylesheet" type="text/css" media="all" />

</head>
<body>
<div class="userappointments_line"></div>
<div class="userappointments_arabicner">
<img src="images/appointment_missed.png" width="30" height="30" style="float:left; margin-left:25px;">
<span style="float:left; margin-top:3px; margin-left:10px;font-family:georgia;">Weekly Calls Report</span>
<span style="float:right; margin-top:3px; margin-left:10px;font-family:georgia;">
<s:if test="%{objAppointmentDTOsList.size!=0}">
<a href="sendEod.action?type=weekly&personType=caller">
<input value="Send" type="button"></a></s:if></span> </div>
<div class="userappointments_line"></div>
<div class="userappointments_line" style="margin-top:10px;"></div>
<font color="green">Count Of Records:<s:property value="objAppointmentDTOsList.size()"/></font>
<div class="userappointments">

    <div class="userbusinessname" style="width:60px;">
    SNO
    </div>
	<div class="userbusinessname" style="width:114px;">
    Business Contact
    </div>
    <div style=" width:90px; height:auto; float:left; margin-left:10px; ">
   Call Time
    </div>
        <div class="user_appointmentfixedby">
   Call Status
    </div>
    <div class="user_appointmenttime" style="width: 290px;">
   Comments
    </div>
  
 
    
</div>

<div class="userappointments_line"></div>
<s:if test="%{objAppointmentDTOsList.size!=0}">
<!-- overflow-y: scroll;overflow-x: hidden; height:450px; width: 788px; -->
<div style="height: 300px; overflow-y: scroll;overflow-x: hidden; width: 720px; float:left" >
<s:iterator value="objAppointmentDTOsList" id="a" status="c">
<div class="userappointments_arabicner1">

  <div class="userbusinessname" style="width:60px;">
		
		    <s:property value="%{#c.count}"/>
		
  </div>


 <div class="userbusinessname" style="width:114px;">
		<s:if test="%{#a.businessContact!=null && #a.businessContact!=''}">
		    <s:property value="#a.businessContact"/>
		</s:if>
		<s:else>
		--------------------------
		</s:else>
  </div>
   
    <div style="width:90px; height:auto; float:left; margin-left:10px;">
	    <s:if test="%{#a.appointmentDateTime!=null && #a.appointmentDateTime!=''}">
	    	<s:property value="#a.appointmentDateTime"/>
	    </s:if>
	    <s:else>
			---------------------------
		</s:else>
    </div>
       <div class="user_appointmentfixedby">
     <s:if test="%{#a.callStatus!=null && #a.callStatus!=''}">
	   <s:property value="#a.callStatus"/>
	 </s:if>
	 <s:else>
	 ----------------------------
	 </s:else>
    </div>
    <div class="user_appointmenttime" style="width: 290px;">
    <s:if test="%{#a.comments!=null && #a.comments!=''}">
    	<s:property value="#a.comments"/>
    </s:if>
    <s:else>
    ----------------------------
    </s:else>
    </div>
  
</div>

</s:iterator>
</div>
</s:if>
<s:else>
<div class="userappointments_arabicner1" style="color:red;font-family: georgia;font-weight: bold">Sorry No Records Are  Available....</div>
</s:else>
</body>
</html>