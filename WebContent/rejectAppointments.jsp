<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="css/css.css" rel="stylesheet" type="text/css" media="all" />

</head>
<body>
<div class="userappointments_line"></div>
<div class="userappointments_arabicner">
<img src="images/appointment_missed.png" width="30" height="30" style="float:left; margin-left:25px;"><span style="float:left; margin-top:3px; margin-left:10px;"> To Day Your Appointments</span> </div>
<div class="userappointments_line"></div>
<div class="userappointments_line" style="margin-top:10px;"></div>

<div class="userappointments">
	<div class="userbusinessname" style="width:140px;">
    Business Name
    </div>
    <div class="user_appointmentplace">
    Appointment Place
    </div>
    
    <div class="user_appointmenttime">
    Appointment Time
    </div>
    <div class="user_appointmentfixedby">
   Rejected by
    </div>
     <div class="user_appointmentplace" style="width:140px;">
    Reason
    </div>
</div>

<div class="userappointments_line"></div>
<s:if test="%{objCheckAppointmentDTOList.size!=0}">
<s:iterator value="objCheckAppointmentDTOList" id="a">
<div class="userappointments_arabicner1">
<div class="userbusinessname" style="width:140px;">
    <s:property value="#a.businessName"/>

  </div>
    <div class="user_appointmentplace">
    <s:property value="#a.businessArea"/>
    </div>
   
    <div class="user_appointmenttime">
    <s:property value="#a.appointmentDateTime"/>
    </div>
    <div class="user_appointmentfixedby">
    <s:if test="%{#a.salesPersonName!=null && #a.salesPersonName!=''}">
    <s:property value="#a.salesPersonName"/>
    </s:if>
    <s:else>
    ........
    </s:else>
    </div>
   <div class="user_appointmenttime" style="width:140px;">
      <s:if test="%{#a.comments!=null && #a.comments!=''}">
    	<s:property value="#a.comments"/>
    </s:if>
     <s:else>
	   		 ........
	 </s:else>
    </div>

</div>

</s:iterator>
</s:if>
<s:else>
<div class="userappointments_arabicner1" style="color:red;font-family: georgia;font-weight: bold">Sorry No Records Are  Available....</div>
</s:else>
</body>
</html>