<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="css/css.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="css/jquery-ui123.css" />	 
<%-- <script type="text/javascript" src="js/jquery-ui.min.js"></script> --%>
<script src="js/jquery.min.js"></script>


<script src="js/jquery.autocomplete1.js"></script>
<link href="http://dialuscss.dialus.com/dialus.css" rel="stylesheet" type="text/css" media="all"/>

<script type="text/javascript">
//var name=$('#name_id').val();



$('#name_id').autocomplete("AllAppointmentAuto.action");

 function submit_button()
 {
	 var tellcallername=$("#tellcallername").val();

	 $('#name_div').load("allAppointments.action?tellcallername="+tellcallername+"&param=ram"); 
	 $("#appointments_by_nam").hide();
	 $("#name_div1").hide();
	  $("#userappointments_div").hide();
 }

 

</script>
</head>
<body>
<div class="userappointments_line"></div>
<div class="userappointments_arabicner" id="name_div1">
<a style="cursor: pointer;color:#008abb; margin: 0 0 0 50px; text-decoration: none;" href="adminHome.jsp" id="home_link">  Home </a>
<img src="images/appointment_missed.png" width="30" height="30" style="float:left; margin-left:25px;"><span style="float:left; margin-top:3px; margin-left:10px;"> All  Appointments</span> </div>
<div class="userappointments_line"></div>
<div class="userappointments_line" style="margin-top:10px;"></div>

 <div id="appointments_by_nam" >
    		  
    		  <div class="main_div_middle_textfields_main" style="width:600px;">
								 
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:150px; text-align:right; margin-left:30px;">
								       	   		<b style="color:red">&lowast;</b>Search Name :
								            </div>
                                            
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px; width:219px;">
								           		
								           		 <input type="text" id="name_id" name="name"/>
								            </div>
								            
								            <div class="main_div_middle_textfields_main_right" style="margin-left:5px; width:80px;">
								           		<%--  <s:textfield name="reportTodate" id="reportTodate"  cssClass="reportTodate"></s:textfield> --%>
								           		 <s:submit id="submit_name"  onclick="submit_button()"></s:submit>
								            </div>
								 
	        </div>
    		  
    		  
    		  
    		  
  </div>








<div class="userappointments" id="userappointments_div">
	<div class="userbusinessname">
    Business Name
    </div>
    <div class="user_appointmentplace">
    Appointment Place
    </div>
    <div class="user_appointmenttime">
    Appointment Time
    </div>
    <div class="user_appointmentfixedby">
    Fixed by
    </div>
</div>

<div id="name_div">

<div class="userappointments_line"></div>
<s:if test="%{objAppointmentDTOList.size!=0}">
<s:iterator value="objAppointmentDTOList" id="a">
<div class="userappointments_arabicner1">
<div class="userbusinessname">
    <s:property value="#a.businessName"/>

  </div>
    <div class="user_appointmentplace">
    <s:property value="#a.businessArea"/>
    </div>
    <div class="user_appointmenttime">
    <s:property value="#a.appointmentDateTime"/>
    </div>
    <div class="user_appointmentfixedby">
    <s:if test="%{#a.telecallerName!=null && #a.telecallerName!=''}">
    <s:property value="#a.telecallerName"/>
    </s:if>
    <s:else>
    ........
    </s:else>
    </div>
    <div class="user_appointmentalert">
    <input type="text" id="tellcallername" value='<s:property value="#a.telecallerName"/>'/>
    <a href="allAppointments.action?appointmentCity=<s:property value="#a.businessCity"/>&appointmentId=<s:property value="#a.appointmentId"/>&type=one&appointmentPlace=<s:property value="#a.businessArea"/>&tellcallername=<s:property value="#a.telecallerName"/>&usertype=admin&param=all1" id="allot" style="color:#fff; font-weight:bold; background-color:#008abb; text-align:center; padding:5px; border:none;">Allot</a> 
   <%-- <a href="loadAppointments.action?appointmentCity=<s:property value="#a.businessCity"/>&appointmentId=<s:property value="#a.appointmentId"/>&type=one&appointmentPlace=<s:property value="#a.businessArea"/>&tellcallername=<s:property value="#a.telecallerName"/>&usertype=admin" id="allot" style="color:#fff; font-weight:bold; background-color:#008abb; text-align:center; padding:5px; border:none;">Allot</a>--%>
    </div>

</div>

</s:iterator>
</s:if>
<s:else>
<div class="userappointments_arabicner1" style="color:red;font-family: georgia;font-weight: bold">Sorry No Records Are  Available....</div>
</s:else>
</div>
</body>
</html>