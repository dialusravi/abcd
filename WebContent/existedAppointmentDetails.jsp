<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="css/css.css" rel="stylesheet" type="text/css" media="all" />

</head>
<body>
<div class="load_appointments" style="">
	
	<div class="load_appointments_headding" style="width:720px;border-radius:14px 14px 0 0;">
    <img src="images/appointment_missed.png" width="30" height="30" style="float:left; margin-left:15px;"> 
    <span style="float:left; margin-left:10px; margin-top:4px;"> Last 10 Appointment Details On This Business Detail</span>
    </div>
    
    
    <s:iterator value="objAppointmentDTOList" id="a" status="no">
    
    <div class="load_appointments_arabicner" style="width:700px;padding:5px 10px;margin-bottom: 10px;">
    <div class="load_appointments_arabicner101" style="width:410px;">
    <s:hidden name="usertype" value="user"></s:hidden>
	<s:hidden name="objCheckAppointmentDTO.appointmentId" value="%{#a.appointmentId}"></s:hidden>
		 SNO:<s:property value="%{#no.count}"/>			      
    	<div class="load_appointments_arabicnername" style="width:400px;">
      Meet:<s:property value="%{#a.contactPerson}"/>
        </div>
        <div class="load_appointments_arabicnerplece" style="width:400px;">
        Near : <s:property value="%{#a.appointmentPlace}"/>
        </div>
        <div class="load_appointments_arabicnertime" style="width:400px;">
       Contact Number : <s:property value="%{#a.contactNumber}"/> 
        </div>
           <div class="load_appointments_arabicnertime" style="width:400px;">
       Contact Person : <s:property value="%{#a.contactPerson}"/> 
        </div>
         <div class="load_appointments_arabicnertime" style="width:400px;">
      Business Name : <s:property value="%{#a.businessName}"/> 
        </div>
           <div class="load_appointments_arabicnertime" style="width:400px;">
      Comments : <s:property value="%{#a.comments}"/> 
        </div>
        <div class="load_appointments_arabicnertime" style="width:400px;">
      Contacted By : <s:property value="%{#a.telecallerName}"/> 
        </div>
      </div>
        <div class="load_appointments_arabicner122" style="width:180px; margin-top:10px; color:#ff6600">
        <s:if test="%{#a.appointmentDate!='1900-01-01' && #a.callBackDate=='1900-01-01'}">
	         <p style="color:green;white-space: nowrap;">Call Status: Appointment Fixed</p><br/>
	        <p style="color:#f30909;white-space: nowrap;">Appointment On:<s:property value="%{#a.appointmentDate}"/></p><br/>
	        <p style="color:#f30909;white-space: nowrap;">Appointment At:<s:property value="%{#a.appointmentTime}"/></p>
         </s:if>
         <s:elseif test="%{#a.appointmentDate=='1900-01-01' && #a.callBackDate!='1900-01-01'}">
         
          <p style="color:#f30909;white-space: nowrap;font-weight: bold;">Call Status:Call Back</p><br/>
	        <p style="color:#f30909;white-space: nowrap;">Call Back On:<s:property value="%{#a.callBackDate}"/></p><br/>
         </s:elseif>
         <s:elseif test="%{#a.appointmentDate=='1900-01-01' && #a.callBackDate=='1900-01-01'}">
         
          <p style="color:#f30909;white-space: nowrap;font-weight: bold;">Call Status:Saved Data</p><br/>
         </s:elseif>
        <a href="updateAppointment.action?appointmentId=<s:property value="#a.appointmentId"/>"><input type="button" value="Update Details" ></a>
        
              </div>
      
  </div>
  
  </s:iterator>
  
  <span style="font-size:20px;color:red;margin-left:290px">Arabic Records</span>
    
  
    <s:iterator value="objAppointmentDTOList1" id="a" status="no">
   
    <div class="load_appointments_arabicner" style="width:700px;padding:5px 10px;margin-bottom: 10px;">
    <div class="load_appointments_arabicner101" style="width:410px;">
    <s:hidden name="usertype" value="user"></s:hidden>
	<s:hidden name="objCheckAppointmentDTO.appointmentId" value="%{#a.appointmentId}"></s:hidden>
		 الرقم المتسلسل:<s:property value="%{#no.count}"/>			      
    	<div class="load_appointments_arabicnername" style="width:400px;">
     اجتماع:<s:property value="%{#a.contactPerson}"/>
        </div>
        <div class="load_appointments_arabicnerplece" style="width:400px;">
        قريب : <s:property value="%{#a.appointmentPlace}"/>
        </div>
        <div class="load_appointments_arabicnertime" style="width:400px;">
       رقم الهاتف : <s:property value="%{#a.contactNumber}"/> 
        </div>
           <div class="load_appointments_arabicnertime" style="width:400px;">
       رقم الشخص : <s:property value="%{#a.contactPerson}"/> 
        </div>
         <div class="load_appointments_arabicnertime" style="width:400px;">
      الاسم التجاري : <s:property value="%{#a.businessName}"/> 
        </div>
           <div class="load_appointments_arabicnertime" style="width:400px;">
      تعليقات : <s:property value="%{#a.comments}"/> 
        </div>
        <div class="load_appointments_arabicnertime" style="width:400px;">
      الاتصال من قبل : <s:property value="%{#a.telecallerName}"/> 
        </div>
      </div>
        <div class="load_appointments_arabicner122" style="width:180px; margin-top:10px; color:#ff6600">
        <s:if test="%{#a.appointmentDate!='1900-01-01' && #a.callBackDate=='1900-01-01'}">
	         <p style="color:green;white-space: nowrap;">حالة الاتصال: Appointment Fixed</p><br/>
	        <p style="color:#f30909;white-space: nowrap;">تمقابلة في :<s:property value="%{#a.appointmentDate}"/></p><br/>
	        <p style="color:#f30909;white-space: nowrap;">مقابلة في:<s:property value="%{#a.appointmentTime}"/></p>
         </s:if>
         <s:elseif test="%{#a.appointmentDate=='1900-01-01' && #a.callBackDate!='1900-01-01'}">
         
          <p style="color:#f30909;white-space: nowrap;font-weight: bold;">حالة الاتصال:Call Back</p><br/>
	        <p style="color:#f30909;white-space: nowrap;">Call Back On:<s:property value="%{#a.callBackDate}"/></p><br/>
         </s:elseif>
         <s:elseif test="%{#a.appointmentDate=='1900-01-01' && #a.callBackDate=='1900-01-01'}">
         
          <p style="color:#f30909;white-space: nowrap;font-weight: bold;">حالة الاتصال:Saved Data</p><br/>
         </s:elseif>
        <a href="arabicupdateAppointment.action?appointmentId=<s:property value="#a.appointmentId"/>"><input type="button" value="Update Details"></a>
        
              </div>
      
  </div>
  
  
  </s:iterator>
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
    <div style="clear:both"></div>
</div>


</body>
</html>