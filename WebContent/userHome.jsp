<%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Saudi Sales Tracking</title>
<link href="css/css.css" rel="stylesheet" type="text/css" media="all" />
<script src="js/jquery-min.js"></script>
<script type="text/javascript" src="date_time.js"></script>
<script type="text/javascript" src="js/salesTrackingValid.js"></script> 
<script type="text/javascript">
function date_time(id)
{
        date = new Date;
        year = date.getFullYear();
        month = date.getMonth();
        months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'Jully', 'August', 'September', 'October', 'November', 'December');
        d = date.getDate();
        day = date.getDay();
        days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
        h = date.getHours();
        if(h<10)
        {
                h = "0"+h;
        }
        m = date.getMinutes();
        if(m<10)
        {
                m = "0"+m;
        }
        s = date.getSeconds();
        if(s<10)
        {
                s = "0"+s;
        }
        result =  ''+d+' '  +months[month]+'  '+year+' -  '+h+':'+m+':'+s;
        document.getElementById(id).innerHTML = result;
        setTimeout('date_time("'+id+'");','1000');
        return true;
}
$(document).ready(function()
{
	$('#success_msg').fadeOut(10000);
	$('#banner_div').show();
	$('#appointments').hide();
	$('#generate_eod_div').hide();
	$('#generate_eod_content_div').hide();
	$('.black_overlay').hide();
	$('#appointment_status').hide();
	$('#appointmentform_div').hide();
	
	
	$("#generate_eod_link").click(function()
			{
				$('#banner_div').hide();
				$('#appointments').hide();
				$('.black_overlay').hide();
				$('#appointment_status').hide();
				$('#appointmentform_div').hide();
				$('#generate_eod_content_div').hide();
				$('#generate_eod_div').show();

			});

	
	$("#appointments_link").click(function()
	{
		$('#banner_div').hide();
		$('.black_overlay').hide();
		$('#appointment_status').hide();
		$('#generate_eod_div').hide();
		$('#generate_eod_content_div').hide();
		$('#appointmentform_div').hide();
		$('#appointments').html("");
		$('#appointments').load("loadUserAppointmentsAction.action?type=fixed"); 
		$('#appointments').show();
	});
	$("#pending_appointments_link").click(function()
			{
				$('#banner_div').hide();
				$('.black_overlay').hide();
				$('#generate_eod_div').hide();
				$('#appointment_status').hide();
				$('#generate_eod_content_div').hide();
				$('#appointmentform_div').hide();
				$('#appointments').html("");
				$('#appointments').load("loadUserAppointmentsAction.action?type=pending"); 
				$('#appointments').show();
			});
	
	
	$(".closeRatingBox").click(function(){
		
		$('#banner_div').show();
		$('#appointments').hide();
		$('#appointmentform_div').hide();
		$('#generate_eod_div').hide();
		$('#generate_eod_content_div').hide();
		$('#appointment_status').hide();
		$('.white_content').fadeOut(1000);
		$('.black_overlay').fadeOut(1000);

	
 });
	
	$("#eodtype").change(function()
			{ 
		        	var eodtype=$("#eodtype").val();
		        	
		        	$('#banner_div').hide();
					$('#appointments').hide();
					$('.black_overlay').hide();
					$('#appointment_status').hide();
					$('#appointmentform_div').hide();
					$('#generate_eod_div').hide();
					$('#generate_eod_content_div').load("generateeod.action?type="+eodtype);
					$('#generate_eod_content_div').show();
					
		        
		    });
    
});

function getAppointmentDetails(appointmentId)
{
	
	$('#appointments').hide();
	$('#appointment_status').hide();
	$('.black_overlay').hide();
	$('#generate_eod_div').hide();
	$('#generate_eod_content_div').hide();
	$('#appointmentform_div').load("loadUserAppointmentsAction.action?appointmentId="+appointmentId+"&type=one");
	$('#appointmentform_div').show();
	
}
function pending(appointmentId)
{
	$('#hiddenappointmentId').val(appointmentId);
	$('#hiddenappointmentType').val("pending");
	$('#banner_div').hide();
	$('#appointments').hide();
	$('#generate_eod_div').hide();
	$('#appointmentform_div').hide();
	$('#generate_eod_content_div').hide();
	$('.black_overlay').show();
	$('#appointment_status').show();
	
	
}
function completed(appointmentId)
{
	$('#hiddenappointmentId').val(appointmentId);
	$('#hiddenappointmentType').val("complete");
	$('#banner_div').hide();
	$('#appointments').hide();
	$('#appointmentform_div').hide();
	$('#generate_eod_content_div').hide();
	$('#generate_eod_div').hide();
	$('.black_overlay').show();
	$('#appointment_status').show();
}
function reject(appointmentId)
{
	$('#hiddenappointmentId').val(appointmentId);
	$('#hiddenappointmentType').val("reject");
	$('#banner_div').hide();
	$('#appointments').hide();
	$('#appointmentform_div').hide();
	$('#generate_eod_div').hide();
	$('#generate_eod_content_div').hide();
	$('.black_overlay').show();
	$('#appointment_status').show();
}
</script>

</head>

<body>
<div class="top_bg">
	<div class="topbg_arabicner">
   	  <div class="topbg_arabicner_left">
        	<div class="topbg_arabicner_left_top">
            SAUDI SALES TRACKING
            </div>
            <div class="topbg_arabicner_left_bottom">
             <span id="date_time"></span>
            <script type="text/javascript">window.onload = date_time('date_time');</script>
          </div>
      </div>
      <div class="topbg_arabicner_right">
        	<div class="topbg_arabicner_right_left">
       	  <img src="images/dialus_small.png" width="28" height="27" /> </div>
            <div class="topbg_arabicner_right_right">
            9666 888 888
            </div>
      </div>
    </div>
</div>
<div style="clear:both"></div>
<div class="wrapper">
<div class="logopart">
	<div class="logopart_left">
    <img src="images/logo.jpg" width="225" height="90" /> 
    </div>
  <!--   <div class="logopart_right">
    
    	<ul>
        
        <li>About Us</li>
         <li>|</li>
        <li>Services</li>
         <li>|</li>
        <li>Gallery</li>
          <li>|</li>
        <li>Products</li>
          <li>|</li>
        <li>Contact Us</li>
        </ul>
    </div> -->
</div>
<s:if test="%{#request.success_msg!=null && #request.success_msg!=''}">
		<div style="text-align: center;width:980px;" id="success_msg"> <font style="color: green;font-family: georgia;font-weight: bold;"><s:property value="%{#request.success_msg}"/></font></div>
	</s:if>
<div class="contentpart">
	<div class="content_left">
    	
        <div id="login_links">
	        <div class="content_left_headding">
		  <div class="content_left_text">
            Menu
          </div>
      </div>
	       <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;" id="appointments_link" >Today Appointments</a>
	        	</div>
	       </div>
	         <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;" id="pending_appointments_link" >Pending Appointments</a>
	        	</div>
	       </div>
	        <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;" id="generate_eod_link" >Generate EOD Report</a>
	        	</div>
	       </div>
	       
	       <!-- <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;" id="notification_link" >Notifications</a>
	        	</div>
	       </div>
	       <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;" id="livechat_link" >Live Chat</a>
	        	</div>
	       </div>
	        <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;" id="profile_link" >Your Profile</a>
	        	</div>
	       </div>
	       
	        <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;" id="settings_link" >Settings</a>
	        	</div>
	       </div>
	        <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;" id="reports_link" >Reports</a>
	        	</div>
	       </div>--> 
	       <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a href="LogOut.action" style="cursor: pointer;text-decoration: none;color:#f30909;" id="logout_link">Log Out</a>
	        	</div>
	       </div>
      </div>
      
    </div>
    
	   	  	   
    <div class="content_right">
    
	   	  <div class="banner_part" id="banner_div">
	        	
	            
	            <div class="banner_part">
	             </div>
	            
	      </div>
	     

   		<div id="appointments"></div>
   		
	     <div id="appointmentform_div"></div>
	     <div class="black_overlay"></div>
	     <div id="appointment_status" class="white_content">
	     <a href = "#" class = "closeRatingBox" style="float:right;">
	<img src="http://dialusimages.dialus.com/images/close_4.png" alt="" style="margin-right:-15px; margin-top:-15px;" /></a>
	     	<s:form action="submitAppointment">
		     	<s:hidden name="objCheckAppointmentDTO.appointmentId" id="hiddenappointmentId"></s:hidden>
		     	<s:hidden name="objCheckAppointmentDTO.appointmentType" id="hiddenappointmentType"></s:hidden>
		     	<s:textarea name="objCheckAppointmentDTO.comments" cssStyle="width:400px;height:70px;"  id="appcomments"    label="Comments"></s:textarea>
	     	<s:submit id="appointmentstatus_but"></s:submit>
	     	
	     	</s:form>
	     
	     </div>
	     
	     <div id="generate_eod_div">
	     
	     	 <div class="main_div_middle">
			         
		                       	<div class="appointmentform_left_left">
		                         		  Select Eod Type:
		                        </div>
		                            
		                   		<div class="appointmentform_left_right">
		                     			<select id="eodtype">
					      					<option value="123">-----Select-------</option>
					      					<option value="daily">Daily</option>
						      				<option value="weekly">Weekly</option>
						      				<option value="monthly">Monthly</option>
	      								</select>
		                   		</div>
			         
		           </div>
	     
	     </div>
	     <div id="generate_eod_content_div"></div>
</div>
</div>
</div>
<div class="fotter">
	<div class="fotter_arabicner">
     All Rights Reserved - Copyright © Dialus.com 2010-2013
    </div>
</div>
</body>
</html>
