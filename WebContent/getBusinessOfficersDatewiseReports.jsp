<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="css/css.css" rel="stylesheet" type="text/css" media="all" />

</head>
<body>
<div class="userappointments_line"></div>
<div class="userappointments_arabicner">
<img src="images/appointment_missed.png" width="30" height="30" style="float:left; margin-left:25px;">
<span style="float:left; margin-top:3px; margin-left:10px;font-family:georgia;">Business Dev Officer Report </span>( <s:property value="%{objAppointmentDTOList.size}"/>)
 </div>
<div class="userappointments_line"></div>
<div class="userappointments_line" style="margin-top:10px;"></div>

<div class="userappointments">
	<div class="userbusinessname" style="width:240px;">
    Business Detail
    </div>
    <div class="user_appointmentplace">
 	Appointment Time
    </div>
  <div class="user_appointmentfixedby" style="width:150px;">
   Appointment Place
   </div>
    <div class="user_appointmentfixedby" style="white-space: nowrap;">
   Appointment Status
   </div>
   
  
 
    
</div>

<div class="userappointments_line"></div>
<s:if test="%{objAppointmentDTOList.size!=0}">
<!-- overflow-y: scroll;overflow-x: hidden; height:450px; width: 788px; -->
<div style="height: 300px; overflow-y: scroll;overflow-x: hidden; width: 720px; float:left" >
<s:iterator value="objAppointmentDTOList" id="a">
<div class="userappointments_arabicner1">
<div class="userbusinessname" style="width:240px;">
		<s:if test="%{#a.businessName!=null && #a.businessName!=''}">
		    <s:property value="#a.businessName"/>
		</s:if>
		<s:else>
		--------------------------
		</s:else>
  </div>
    <div class="user_appointmentplace">
	    <s:if test="%{#a.appointmentDateTime!=null && #a.appointmentDateTime!=''}">
	    	<s:property value="#a.appointmentDateTime"/>
	    </s:if>
	    <s:else>
			---------------------------
		</s:else>
    </div>
       <div class="user_appointmentfixedby" style="width:150px;">
     <s:if test="%{#a.businessArea!=null && #a.businessArea!=''}">
	   <s:property value="#a.businessArea"/>
	 </s:if>
	 <s:else>
	 ----------------------------
	 </s:else>
    </div>
    <div class="user_appointmenttime">
    <s:if test="%{#a.callStatus!=null && #a.callStatus!=''}">
    	<s:property value="#a.callStatus"/>
    </s:if>
    <s:else>
    ----------------------------
    </s:else>
    </div>
    
    
  
</div>

</s:iterator>
</div>
</s:if>
<s:else>
<div class="userappointments_arabicner1" style="color:red;font-family: georgia;font-weight: bold">Sorry No Records Are  Available....</div>
</s:else>
</body>
</html>