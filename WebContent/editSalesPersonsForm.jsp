<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<link href="http://dialuscss.dialus.com/dialus.css" rel="stylesheet" type="text/css" media="all"/>
<script type="text/javascript" src="js/salesTrackingValid.js"></script> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script>
	
	
	jQuery(function(){
		 
		
			
		
		 $('#editsalesperson_state').autocomplete('CityautoCompleteAction.action?variable=state');
			$('#editsalesperson_city').autocomplete('CityautoCompleteAction.action',
					
					{
				  extraParams:
					
				        {
					  businessState: function () {
				                return $("#editsalesperson_state").val();
				            },
				            
				            
				            variable: function () {
				                return "city";
				            }
				
				        }
					}
			
			
			);
			
			/*area starts here  */
			
			
			
			/*Start of area auto complete  */
		$('#editsalesperson_area').autocomplete('CityautoCompleteAction.action',
					
					{
				  extraParams:
					
				        {
					  businesCity: function () {
				                return $("#editsalesperson_city").val();
				            },
				            
				            
				            variable: function () {
				                return "area";
				            }

				        }
					}
			
			
			);
	});
			</script>
</head>
<body>

						      
						      	<s:form action="editUsersAction" theme="simple">
								    	<s:hidden name="objUsersOperationsDTO.usertype" value="salesperson"></s:hidden>
								    	<s:hidden name="objUsersOperationsDTO.userId" value="%{objUsersOperationsDTO.userId}"></s:hidden>
								    	
								        <div class="main_div_middle">
								         <div class="main_div_middle_textfields_main" style="text-align:center;background: #008ABB;width: 100%;color:#fff;padding: 8px 0px;margin-top:-10px;border-top-left-radius:10px;border-top-right-radius:10px;font-family: georgia;">
											          Business Dev Officers  Details
											        </div>
									        <div class="appointmentform_left1" style="margin-left:200px;">
                                	<div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b> Business Dev Officer Name :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objUsersOperationsDTO.userName"  value="%{objUsersOperationsDTO.userName}"  maxlength="45"   id="editsalesperson_name"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b> Business Dev Officer State :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userState" maxlength="40"  value="%{objUsersOperationsDTO.userState}"   id="editsalesperson_state"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                      <b style="color:red">&lowast;</b> Business Dev Officer City :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userCity" maxlength="40"  value="%{objUsersOperationsDTO.userCity}"   id="editsalesperson_city"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                     <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b> Business Dev Officer Area :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objUsersOperationsDTO.userArea" maxlength="40"  value="%{objUsersOperationsDTO.userArea}"    id="editsalesperson_area"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                      <b style="color:red">&lowast;</b> Business Dev Officer Mobile :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objUsersOperationsDTO.userMobile" maxlength="10"  value="%{objUsersOperationsDTO.userMobile}"     id="editsalesperson_mobile"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                         Business Dev Officer Mobile2 :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userMobile1" maxlength="10"  value="%{objUsersOperationsDTO.userMobile1}"     id="editsalesperson_mobile1"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                      Business Dev Officer Mail Id :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userMailId" maxlength="60"  value="%{objUsersOperationsDTO.userMailId}"    id="editsalesperson_mailid"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                      <b style="color:red">&lowast;</b>Password :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userPassword" maxlength="10"  value="%{objUsersOperationsDTO.userPassword}"    id="editsalesperson_password"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                    <b style="color:red">&lowast;</b>Confirm Password:
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield   maxlength="10"  value="%{objUsersOperationsDTO.userPassword}"    id="editsalesperson_confirmpassword"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                   
                                      
                                      <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                    
                                        </div>
                                        
                                        <div class="appointmentform_left_right" style="float: right;">
                                      <s:submit value="Update  UserDetails" id="editsalesperson_but"></s:submit>
                                        </div>
                                    </div>
                                    </div>
									            
									            
									          
									        </div>
								     
								     </s:form>
							
				      
				    
</body>
</html>