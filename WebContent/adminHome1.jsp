<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Sales Tracking</title>
<%-- <style type="text/css">
#map_div {
width:500px;
height:380px;
}
</style> --%>
<link href="css/css.css" rel="stylesheet" type="text/css" media="all" />
<script src="js/jquery-min.js"></script>
<link rel="stylesheet" href="css/jquery-ui123.css" />	 
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/redmond/jquery-ui.css" />

<!-- Plugin files below -->
<link rel="stylesheet" type="text/css" href="css/jquery.ptTimeSelect.css" />
<script type="text/javascript" src="js/jquery.ptTimeSelect.js"></script>

 <script type="text/javascript" src="js/salesTrackingValid.js"></script> 


<script type="text/javascript">


function show_date()
{
	
	 $( ".reportTodate123" ).datepicker();
	
	}
function submit_button_clicked()
{
	

	
		 $(".error").hide();
	     var hasError = false;
	     var todaydate=$('#today_date').val();
	   
	     if( todaydate=='')
	        {
	    	 	
	    	 	$("#today_date").after('<span style="color: red;white-space:nowrap;" class="error">Please Select Date.....</span>');
	    	
	    	 	hasError = true;
	        }
	    if(hasError == true){
	    	return false; 
	    }
	    
		    else
		    {
		    	
		    	
		    	$('#appointments').load("loadAppointments.action?todaydate="+todaydate);
		    	$('#appointments').show();
		    	
		    	$('#appointments_by_date').hide();
		    	
		    	
		    
		    	return true; 
		    }
			
			
	
	
	
	}

function date_time(id)
{
        date = new Date;
        year = date.getFullYear();
        month = date.getMonth();
        months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'Jully', 'August', 'September', 'October', 'November', 'December');
        d = date.getDate();
        day = date.getDay();
        days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
        h = date.getHours();
        if(h<10)
        {
                h = "0"+h;
        }
        m = date.getMinutes();
        if(m<10)
        {
                m = "0"+m;
        }
        s = date.getSeconds();
        if(s<10)
        {
                s = "0"+s;
        }
        result =  ''+d+' '  +months[month]+'  '+year+' -  '+h+':'+m+':'+s;
        document.getElementById(id).innerHTML = result;
        setTimeout('date_time("'+id+'");','1000');
        return true;
}







$(document).ready(function()
{
	$( ".reportTodate123" ).datepicker(); 
	
		$('#success_msg').fadeOut(5000);
		$('#banner_div').show();
		$('#pending_appointments_div,#rejected_appointments_div,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#salesperson_div').hide();
		$('#telecaller_div').hide();
		$('#deletetelecallers_div').hide();
		$('#deletesalespersons_div').hide();
		$('#editsalespersons_div').hide();
		$("#calllist_div").hide();
		$("#saleslist_div").hide();
		$("#datewisereport_div").hide();
		$("#datewiseReportsResults_div").hide();
		$('#edittelecallers_div').hide();
		$('#editsalespersonsdetails_div').hide();
		$('#edittelecallersdetails_div').hide();
		$('#appointments').hide();
		$('#appointments_by_date').hide();
		
		$("#appointments_link").click(function()
		{
			
			$('#deletetelecallers_div').hide();
			$('#deletesalespersons_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$('#editsalespersonsdetails_div').hide();
			$('#edittelecallersdetails_div').hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$("#calllist_div").hide();
			$("#saleslist_div").hide();
			$("#datewisereport_div").hide();
			$("#datewiseReportsResults_div").hide();
			$("#business_div").hide();
			$('#pending_appointments_div,#rejected_appointments_div,#banner_div,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();  	
			/* $('#appointments').load("loadAppointments.action?type=all"); */
			$('#appointments').hide();
		
			$('#appointments_by_date').show();
		});
		
		
		$("#rejected_appointments_link").click(function()
		{
			$('#deletetelecallers_div').hide();
			$('#deletesalespersons_div').hide();
			$('#editsalespersonsdetails_div').hide();
			$('#edittelecallersdetails_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$("#calllist_div").hide();
			$('#telecaller_div').hide();
			$("#datewiseReportsResults_div").hide();
			$("#datewisereport_div").hide();
			$("#saleslist_div").hide();
			$('#salesperson_div').hide();
			$("#business_div").hide();
			$('#appointments_by_date').hide();
			$('#pending_appointments_div,#appointments,#banner_div,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();  	
			//$('#rejected_appointments_div').load("loadAppointments.action?type=all");
			$('#rejected_appointments_div').load("rejectAppointments.action?type=reject");
			$('#rejected_appointments_div').show();
		});
		
		$("#pending_appointments_link").click(function()
				{
					$('#deletetelecallers_div').hide();
					$('#deletesalespersons_div').hide();
					$('#editsalespersonsdetails_div').hide();
					$('#edittelecallersdetails_div').hide();
					$('#editsalespersons_div').hide();
					$('#edittelecallers_div').hide();
					$("#datewiseReportsResults_div").hide();
					$("#datewisereport_div").hide();
					$('#appointments_by_date').hide();
					$('#telecaller_div').hide();
					$('#salesperson_div').hide();
					$("#calllist_div").hide();
					$('#rejected_appointments_div,#pending_appointments_div,#appointments,#banner_div,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();  	
					//$('#rejected_appointments_div').load("loadAppointments.action?type=all");
					$('#pending_appointments_div').load("pendingAppointments.action?type=pending");
					$('#pending_appointments_div').show();
					$("#saleslist_div").hide();
					$("#business_div").hide();
				});
		
		
		$("#edituser_link").click(function()
		{
			$('#deletetelecallers_div').hide();
			$('#deletesalespersons_div').hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$('#editsalespersonsdetails_div').hide();
			$('#edittelecallersdetails_div').hide();
			$("#datewisereport_div").hide();
			$("#saleslist_div").hide();
			$('#pending_appointments_div,#banner_div,#rejected_appointments_div,#appointments,#appointmentform_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
			//$('#edituser_div').load("loadAppointments.action?type=all");
			$('#edituser_div').show();
			$("#datewiseReportsResults_div").hide();
			$('#appointments_by_date').hide();
			$("#calllist_div").hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$("#saleslist_div").hide();
			$("#business_div").hide();
		});
		$("#deleteuser_link").click(function()
		{
			$('#deletetelecallers_div').hide();
			$('#deletesalespersons_div').hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$('#editsalespersons_div').hide();
			$("#datewisereport_div").hide();
			$('#edittelecallers_div').hide();
			$("#datewiseReportsResults_div").hide();
			$("#calllist_div").hide();
			$("#saleslist_div").hide();
			$('#editsalespersonsdetails_div').hide();
			$('#appointments_by_date').hide();
			$('#edittelecallersdetails_div').hide();
			$("#business_div").hide();
			$('#pending_appointments_div,#edituser_div,#banner_div,#rejected_appointments_div,#appointments,#appointmentform_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		//	$('#deleteuser_div').load("loadAppointments.action?type=all");
			$('#deleteuser_div').show();
			
			
		});
		$("#createuser_link").click(function()
		{
			$('#deletetelecallers_div').hide();
			$('#deletesalespersons_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$('#editsalespersonsdetails_div').hide();
			$('#edittelecallersdetails_div').hide();
			$("#datewisereport_div").hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$("#calllist_div").hide();
			$("#datewiseReportsResults_div").hide();
			$('#salesperson_div').hide();
			$("#saleslist_div").hide();
			$('#telecaller_div').hide();
			$("#business_div").hide();
			$('#pending_appointments_div,#deleteuser_div,#edituser_div,#banner_div,#rejected_appointments_div,#appointments,#appointmentform_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
			//$('#createuser_div').load("loadAppointments.action?type=all");
			$('#createuser_div').show();
			
		});
		
		$("#telecallerreport_link").click(function()
				{
					$('#deletetelecallers_div').hide();
					$('#deletesalespersons_div').hide();
					$('#editsalespersons_div').hide();
					$('#edittelecallers_div').hide();
					$('#editsalespersonsdetails_div').hide();
					$('#edittelecallersdetails_div').hide();
					$("#datewisereport_div").hide();
					$('#telecaller_div').hide();
					$("#datewiseReportsResults_div").hide();
					$('#appointments_by_date').hide();
					$("#calllist_div").hide();
					$('#salesperson_div').hide();
					$("#saleslist_div").hide();
					$("#business_div").hide();
					$('#pending_appointments_div,#createuser_div,#deleteuser_div,#edituser_div,#banner_div,#rejected_appointments_div,#appointments,#appointmentform_div,#calllist_div,#salesreport_div').hide();
					//$('#telecallerreport_div').load("loadAppointments.action?type=all");
					$('#telecallerreport_div').show();
				});
		
		
		
		$("#salesreport_link").click(function()
		{
			$('#deletetelecallers_div').hide();
			$('#deletesalespersons_div').hide();
			$('#editsalespersons_div').hide();
			$("#datewisereport_div").hide();
			$('#edittelecallers_div').hide();
			$("#calllist_div").hide();
			$('#editsalespersonsdetails_div').hide();
			$('#edittelecallersdetails_div').hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$("#datewiseReportsResults_div").hide();
			$("#saleslist_div").hide();
			$('#appointments_by_date').hide();
			$("#business_div").hide();
			$('#pending_appointments_div,#calllist_div,#telecallerreport_div,#createuser_div,#deleteuser_div,#edituser_div,#banner_div,#rejected_appointments_div,#appointments,#appointmentform_div').hide();
			//$('#telecallerreport_div').load("loadAppointments.action?type=all");
			$('#salesreport_div').show();
		});
		
		
		$("#salesperson_mobile").blur(function()
				{
					var businessMob1=$("#salesperson_mobile").val();	
					if(businessMob1!="")
					{
						$("#mbnoExist").load("mobStatus.action?salesperson_mobile="+businessMob1+"&type=sales");
						$("#mbnoExist").show();
						$("#mbnoExist").fadeOut(10000);
					}
				});
		
		$("#telecaller_mobile").blur(function()
				{
					var businessMob1=$("#telecaller_mobile").val();	
					if(businessMob1!="")
					{
						$("#telecallermbnoExist").load("telecallermobStatus.action?salesperson_mobile="+businessMob1+"&type=telecaller");
						$("#telecallermbnoExist").show();
						$("#telecallermbnoExist").fadeOut(10000);
					}
				});
		
				$("#getTelecallerReport").click(function()
				{
							 $(".error").hide();
						     var hasError = false;
						     var report=$("#reporttype").val();
						     var person= $("#telecallername").val();
						     if(report=='123')
						        {
						    	 	$("#reporttype").after('<span style="color: red;white-space:nowrap;" class="error">Please Select Report Type.....</span>');
						            hasError = true;
						        }
						     if(person=='123')
						     {
						    	 	$("#telecallername").after('<span style="color: red;white-space:nowrap;" class="error">Please Select Telecaller Name.....</span>');
						            hasError = true;
						     }
						    
						    if(hasError == true)
						    { 
						    	
						    	return false; 
						    }
						    else
						    {
						    	$('#deletetelecallers_div').hide();
								$('#deletesalespersons_div').hide();
								$('#editsalespersons_div').hide();
								$('#edittelecallers_div').hide();
								$('#editsalespersonsdetails_div').hide();
								$('#edittelecallersdetails_div').hide();
								$("#datewisereport_div").hide();
								$('#telecaller_div').hide();
								$('#salesperson_div').hide();
								$("#datewiseReportsResults_div").hide();
								$('#pending_appointments_div,#createuser_div,#deleteuser_div,#edituser_div,#banner_div,#rejected_appointments_div,#appointments,#appointmentform_div,#calllist_div,#salesreport_div').hide();
								//$('#telecallerreport_div').load("loadAppointments.action?type=all");
								$('#telecallerreport_div').hide();
								$("#saleslist_div").hide();
								$('#appointments_by_date').hide();
								$("#business_div").hide();
						    	$("#calllist_div").load("getAllReports.action?report="+report+"&person="+person+"&type=caller");
						    	$("#calllist_div").show();
						    	
						    	
						    }
				});
				
				
				
				$("#getSalesPersonReport_but").click(function()
						{
									 $(".error").hide();
								     var hasError = false;
								     var report=$("#salesreporttype").val();
								     var person= $("#salespersonnamereport").val();
								     if(report=='123')
								        {
								    	 	$("#salesreporttype").after('<span style="color: red;white-space:nowrap;" class="error">Please Select Report Type.....</span>');
								            hasError = true;
								        }
								     if(person=='123')
								     {
								    	 	$("#salespersonnamereport").after('<span style="color: red;white-space:nowrap;" class="error">Please Select Telecaller Name.....</span>');
								            hasError = true;
								     }
								    
								    if(hasError == true)
								    { 
								    	
								    	return false; 
								    }
								    else
								    {
								    	$('#deletetelecallers_div').hide();
										$('#deletesalespersons_div').hide();
										$('#editsalespersons_div').hide();
										$('#edittelecallers_div').hide();
										$('#editsalespersonsdetails_div').hide();
										$('#edittelecallersdetails_div').hide();
										$("#datewisereport_div").hide();
										$('#telecaller_div').hide();
										$("#datewiseReportsResults_div").hide();
										$('#salesperson_div').hide();
										$('#pending_appointments_div,#createuser_div,#deleteuser_div,#edituser_div,#banner_div,#rejected_appointments_div,#appointments,#appointmentform_div,#calllist_div,#salesreport_div').hide();
										//$('#telecallerreport_div').load("loadAppointments.action?type=all");
										$('#telecallerreport_div').hide();
								    	$("#calllist_div").hide();
								    	$("#saleslist_div").load("getAllReports.action?report="+report+"&person="+person+"&type=sales");
								    	$("#saleslist_div").show();
								    	$("#business_div").hide();
								    	$('#appointments_by_date').hide();
								    	
								    }
						});
		
		
				
				$("#datewisereport_link").click(function()
						{
							$('#success_msg').fadeOut(10000);
							$('#banner_div').show();
							$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
							$('#salesperson_div').hide();
							$('#telecaller_div').hide();
							$('#deletetelecallers_div').hide();
							$('#deletesalespersons_div').hide();
							$('#editsalespersons_div').hide();
							$("#calllist_div").hide();
							$("#saleslist_div").hide();
							$("#datewiseReportsResults_div").hide();
							$('#edittelecallers_div').hide();
							$('#editsalespersonsdetails_div').hide();
							$('#appointments_by_date').hide();
							$('#edittelecallersdetails_div').hide();
							//$("#datewisereport_div").load("getDateWiseReport.action");
							$("#datewisereport_div").show();
							$("#business_div").hide();
						});
		
				
				$("#business_link").click(function()
						{
							$('#success_msg').fadeOut(10000);
							$('#banner_div').show();
							$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
							$('#salesperson_div').hide();
							$('#telecaller_div').hide();
							$('#deletetelecallers_div').hide();
							$('#deletesalespersons_div').hide();
							$('#editsalespersons_div').hide();
							$("#calllist_div").hide();
							$("#saleslist_div").hide();
							$("#datewiseReportsResults_div").hide();
							$('#edittelecallers_div').hide();
							$('#editsalespersonsdetails_div').hide();
							$('#edittelecallersdetails_div').hide();
							$('#appointments_by_date').hide();
							//$("#datewisereport_div").load("getDateWiseReport.action");
							$("#datewisereport_div").hide();
							$("#business_div").load("BusinessDetailAction.action");
							$("#business_div").show();
							
						});
				
				
				
		
});
/* function getAppointmentDetails(appointmentId,appointmentPlace,appointmentCity)
{
	
	$('#editsalespersonsdetails_div').hide();
	$('#edittelecallersdetails_div').hide();
	$('#appointments').hide();
	$('#deletetelecallers_div').hide();
	$('#deletesalespersons_div').hide();
	$('#editsalespersons_div').hide();
	$('#edittelecallers_div').hide();
	$('#telecaller_div').hide();
	$('#salesperson_div').hide();
	$('#pending_appointments_div,#rejected_appointments_div,#appointments,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
	$('#appointmentform_div').load("loadAppointments.action?appointmentCity="+appointmentCity+"&appointmentId="+appointmentId+"&type=one&appointmentPlace="+appointmentPlace+"&usertype=admin");
	$('#appointmentform_div').show();
	
} */

function loadMap()
{
	function initialize()
	{
		var mapProp =
		{
		  center:new google.maps.LatLng(51.508742,-0.120850),
		  zoom:5,
		  mapTypeId:google.maps.MapTypeId.ROADMAP
		};
		var map=new google.maps.Map(mapProp);
		return map;
	}
	$('#map_div').html(google.maps.event.addDomListener(window, 'load', initialize));
	
}

function selectUser()
{
	
	var person=$('#persontype').val();
	if(person=='salesperson')
	{
		$('#editsalespersonsdetails_div').hide();
		$('#edittelecallersdetails_div').hide();
		$('#banner_div').hide();
		$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#telecaller_div').hide();
		$("#datewisereport_div").hide();
		$("#calllist_div").hide();
		$('#deletetelecallers_div').hide();
		$('#deletesalespersons_div').hide();
		$("#saleslist_div").hide();
		$('#editsalespersons_div').hide();
		$("#datewiseReportsResults_div").hide();
		$('#edittelecallers_div').hide();
		$('#salesperson_div').show();
	}else if(person=='telecaller')
	{
		$('#pending_appointments_div,#editsalespersonsdetails_div').hide();
		$('#edittelecallersdetails_div').hide();
		$('#banner_div').hide();
		$('#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#telecaller_div').show();
		$("#calllist_div").hide();
		$('#salesperson_div').hide();
		$('#deletetelecallers_div').hide();
		$("#datewisereport_div").hide();
		$('#deletesalespersons_div').hide();
		$("#datewiseReportsResults_div").hide();
		$("#saleslist_div").hide();
		$('#editsalespersons_div').hide();
		$('#edittelecallers_div').hide();
	
	}
	else if(person=='123')
	{
		$('#pending_appointments_div,#editsalespersonsdetails_div').hide();
		$('#edittelecallersdetails_div').hide();
		$('#banner_div').hide();
		$("#calllist_div").hide();
		$('#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#telecaller_div').hide();
		$('#salesperson_div').hide();
		$('#deletetelecallers_div').hide();
		$('#deletesalespersons_div').hide();
		$('#editsalespersons_div').hide();
		$("#datewiseReportsResults_div").hide();
		$("#datewisereport_div").hide();
		$('#appointments_by_date').hide();
		$('#edittelecallers_div').hide();
		$("#saleslist_div").hide();
		$('#createuser_div').show();
		$('#persontype').after('<span style="color: red" class="error">Select Any One User Type..... .</span>');
	
	}
}
function selectDeleteUser()
{
	var person=$('#deletepersontype').val();
	if(person=='salesperson')
	{
		$('#pending_appointments_div,#banner_div').hide();
		$('#editsalespersons_div').hide();
		$("#calllist_div").hide();
		$('#edittelecallers_div').hide();
		$('#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#telecaller_div').hide();
		$('#salesperson_div').hide();
		$('#deleteuser_div').hide();
		$('#editsalespersons_div').hide();
		$('#edittelecallers_div').hide();
		$("#datewisereport_div").hide();
		$('#appointments_by_date').hide();
		$("#datewiseReportsResults_div").hide();
		$("#saleslist_div").hide();
		$('#deletetelecallers_div').hide();
		$('#deletesalespersons_div').load("loadUsers.action?type=salesperson");
		$('#deletesalespersons_div').show();
	}else if(person=='telecaller')
	{
		$('#pending_appointments_div,#banner_div').hide();
		$('#editsalespersons_div').hide();
		$("#calllist_div").hide();
		$('#editsalespersons_div').hide();
		$('#edittelecallers_div').hide();
		$('#edittelecallers_div').hide();
		$('#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#telecaller_div').hide();
		$('#salesperson_div').hide();
		$("#datewisereport_div").hide();
		$('#deleteuser_div').hide();
		$("#datewiseReportsResults_div").hide();
		$('#deletesalespersons_div').hide();
		$('#appointments_by_date').hide();
		$('#deletetelecallers_div').load("loadUsers.action?type=telecaller");
		$('#deletetelecallers_div').show();
		
	}
	else if(person=='123')
	{
		$('#banner_div').hide();
		$('#editsalespersons_div').hide();
		$('#edittelecallers_div').hide();
		$("#calllist_div").hide();
		$('#editsalespersons_div').hide();
		$('#edittelecallers_div').hide();
		$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#telecaller_div').hide();
		$('#salesperson_div').hide();
		$("#datewisereport_div").hide();
		$('#createuser_div').hide();
		$('#deleteuser_div').show();
		$('#deletetelecallers_div').hide();
		$("#datewiseReportsResults_div").hide();
		$('#appointments_by_date').hide();
		$('#deletesalespersons_div').hide();
		$("#saleslist_div").hide();
		$('#deletepersontype').after('<span style="color: red" class="error">Select Any One User Type..... .</span>');
	
	}
}
function selectEditUser()
{
	
	var person=$('#editpersontype').val();
	if(person=='salesperson')
	{
		$('#banner_div').hide();
		$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#telecaller_div').hide();
		$('#editsalespersons_div').hide();
		$("#calllist_div").hide();
		$('#edittelecallers_div').hide();
		$('#salesperson_div').hide();
		$('#deleteuser_div').hide();
		$('#deletetelecallers_div').hide();
		$('#deletesalespersons_div').hide();
		$("#datewiseReportsResults_div").hide();
		$("#datewisereport_div").hide();
		$('#edittelecallers_div').hide();
		$("#saleslist_div").hide();
		$('#appointments_by_date').hide();
		$('#editsalespersons_div').load("loadEditUsers.action?type=salesperson");
		$('#editsalespersons_div').show();
	
	}else if(person=='telecaller')
	{
		$('#banner_div').hide();
		$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#telecaller_div').hide();
		$('#salesperson_div').hide();
		$('#editsalespersons_div').hide();
		$('#edittelecallers_div').hide();
		$('#deleteuser_div').hide();
		$('#deletesalespersons_div').hide();
		$("#calllist_div").hide();
		$('#appointments_by_date').hide();
		$('#deletetelecallers_div').hide();
		$("#datewisereport_div").hide();
		$("#datewiseReportsResults_div").hide();
		$("#saleslist_div").hide();
		$('#appointments_by_date').hide();
		$('#edittelecallers_div').show();
		$('#editsalespersons_div').hide();
		$('#edittelecallers_div').load("loadEditUsers.action?type=telecaller");
		$('#edittelecallers_div').show();
		
	}
	else if(person=='123')
	{
		$('#banner_div').hide();
		$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#telecaller_div').hide();
		$('#salesperson_div').hide();
		$('#editsalespersons_div').hide();
		$('#edittelecallers_div').hide();
		$("#calllist_div").hide();
		$('#createuser_div').hide();
		$('#deleteuser_div').hide();
		$('#deletetelecallers_div').hide();
		$("#datewisereport_div").hide();
		$('#deletesalespersons_div').hide();
		$('#editsalespersons_div').hide();
		$("#datewiseReportsResults_div").hide();
		$('#edittelecallers_div').hide();
		$('#appointments_by_date').hide();
		$("#saleslist_div").hide();
		$('#edituser_div').show();
		$('#editpersontype').after('<span style="color: red" class="error">Select Any One User Type..... .</span>');
	
	}
}
function editTelecallers(type,userId)
{
	$('#banner_div').hide();
	$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
	$('#salesperson_div').hide();
	$('#telecaller_div').hide();
	$("#calllist_div").hide();
	$('#deletetelecallers_div').hide();
	$("#datewisereport_div").hide();
	$('#deletesalespersons_div').hide();
	$('#editsalespersons_div').hide();
	$('#edittelecallers_div').hide();
	$('#editsalespersonsdetails_div').hide();
	$("#datewiseReportsResults_div").hide();
	$("#saleslist_div").hide();
	$('#appointments_by_date').hide();
	$('#edittelecallersdetails_div').load("editUser.action?type="+type+"&userId="+userId);
	$('#edittelecallersdetails_div').show();
	
	
}
function editSalesPersons(type,userId)
{
	$('#banner_div').hide();
	$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
	$('#salesperson_div').hide();
	$('#telecaller_div').hide();
	$("#calllist_div").hide();
	$('#deletetelecallers_div').hide();
	$('#deletesalespersons_div').hide();
	$('#editsalespersons_div').hide();
	$("#datewisereport_div").hide();
	$('#edittelecallers_div').hide();
	$("#datewiseReportsResults_div").hide();
	$("#saleslist_div").hide();
	$('#edittelecallersdetails_div').hide();
	$('#appointments_by_date').hide();
	$('#editsalespersonsdetails_div').load("editUser.action?type="+type+"&userId="+userId);
	$('#editsalespersonsdetails_div').show();
	
	
}

</script>

<s:if test="%{#request.operation=='allot'}">
	<script type="text/javascript">
		$(document).ready(function()
		{
			
			$('#deletetelecallers_div').hide();
			$('#deletesalespersons_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$('#editsalespersonsdetails_div').hide();
			$("#calllist_div").hide();
			$('#edittelecallersdetails_div').hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$("#datewisereport_div").hide();
			$("#datewiseReportsResults_div").hide();
			$("#saleslist_div").hide();
			$('#pending_appointments_div,#rejected_appointments_div,#banner_div,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();  	
			$('#appointments').load("loadAppointments.action?todaydate="+todaydate); 
		  /*   $('#appointments').load("loadAppointments.action?type=all"); */
			$('#appointments').show();
			
		});
	</script>
</s:if>
<s:if test="%{#request.update_status!=null}">
	<script type="text/javascript">
		$(document).ready(function()
		{
			$('#banner_div').hide();	
			$('#deletetelecallers_div').hide();
			$('#deletesalespersons_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$("#datewisereport_div").hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$("#calllist_div").hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$("#datewiseReportsResults_div").hide();
			$("#saleslist_div").hide();
			$('#pending_appointments_div,#rejected_appointments_div,#banner_div,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();  
			/* $('#appointments').load("loadAppointments.action?type=all"); */
			$('#appointments').load("loadAppointments.action?todaydate="+todaydate);
			$('#appointments').show();
			
		});
	</script>
</s:if>
<s:if test="%{#request.operation=='deletesalesperson'}">
	<script type="text/javascript">
		$(document).ready(function()
		{
			$('#banner_div').hide();
			$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$('#deleteuser_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$("#datewisereport_div").hide();
			$("#calllist_div").hide();
			$('#deletetelecallers_div').hide();
			$("#datewiseReportsResults_div").hide();
			$("#saleslist_div").hide();
			$('#deletesalespersons_div').load("loadUsers.action?type=salesperson");
			$('#deletesalespersons_div').show();
		});
	</script>
</s:if>
<s:if test="%{#request.operation=='deletetelecaller'}">
	<script type="text/javascript">
		$(document).ready(function()
		{
			$('#banner_div').hide();
			$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$('#deleteuser_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$("#calllist_div").hide();
			$("#saleslist_div").hide();
			$('#deletesalespersons_div').hide();
			$("#datewiseReportsResults_div").hide();
			$('#deletetelecallers_div').load("loadUsers.action?type=telecaller");
			$('#deletetelecallers_div').show();
			
		});
	</script>
</s:if>
<s:if test="%{#request.editoperation=='telecaller'}">
	<script type="text/javascript">
		$(document).ready(function()
		{
			$('#banner_div').hide();
			$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$('#deleteuser_div').hide();
			$('#deletesalespersons_div').hide();
			$('#deletetelecallers_div').hide();
			$('#edittelecallers_div').show();
			$('#editsalespersons_div').hide();
			$("#datewisereport_div").hide();
			$("#datewiseReportsResults_div").hide();
			$("#calllist_div").hide();
			$("#saleslist_div").hide();
			$('#edittelecallers_div').load("loadEditUsers.action?type=telecaller");
			$('#edittelecallers_div').show();
			
		});
	</script>
</s:if>
<s:if test="%{#request.editoperation=='salesperson'}">
	<script type="text/javascript">
		$(document).ready(function()
		{
			$('#banner_div').hide();
			$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
			$('#telecaller_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$('#salesperson_div').hide();
			$('#deleteuser_div').hide();
			$('#deletetelecallers_div').hide();
			$('#deletesalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$("#calllist_div").hide();
			$("#datewisereport_div").hide();
			$("#datewiseReportsResults_div").hide();
			$("#saleslist_div").hide();
			$('#editsalespersons_div').load("loadEditUsers.action?type=salesperson");
			$('#editsalespersons_div').show();
			
		});
	</script>
</s:if>

<script type="text/javascript" src="http://dialusbanners.dialus.com/jquery.autocomplete.js"></script>
<link href="http://dialuscss.dialus.com/dialus.css" rel="stylesheet" type="text/css" media="all"/>
<script>
function changeTextVal(typeValue)
{
	$("#reportUserType").val(typeValue);
}
function changeTextVal1(typeValue)
{
	$("#reportUserType").val(typeValue);
}
$(document).ready(function()
{
	
	
	$("#reportUserName").focus(function()
	{
		var reportUserName=$('#reportUserName').val();
		var reportUserTypeCheck=$('#reportUserType').val();
		$('#reportUserName').autocomplete("getUsers.action?type="+reportUserTypeCheck+"&userName="+reportUserName);
	});
	
	
	$("#getReportBut").click(function()
	{
		var reportUserName=$('#reportUserName').val();
		var reportUserTypeCheck=$('#reportUserType').val();
		var reportTodate=$('#reportTodate').val();
		var reportFromdate=$('#reportFromdate').val();
		$("#datewiseReportsResults_div").load("getUsersDatewiseReports.action?type="+reportUserTypeCheck+"&userName="+reportUserName+"&fromDate="+reportFromdate+"&toDate="+reportTodate);
		$('#banner_div').hide();
		$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#salesperson_div').hide();
		$('#telecaller_div').hide();
		$('#deletetelecallers_div').hide();
		$('#deletesalespersons_div').hide();
		$('#editsalespersons_div').hide();
		$("#calllist_div").hide();
		$("#saleslist_div").hide();
		$("#datewisereport_div").hide();
		$('#edittelecallers_div').hide();
		$('#editsalespersonsdetails_div').hide();
		$('#edittelecallersdetails_div').hide();
		$('#appointments_by_date').hide();
		$("#datewiseReportsResults_div").show();
	});
	
/* 
	$("#submit_date").click(function()
	{
		 $(".error").hide();
	     var hasError = false;
	     var todaydate=$('#today_date').val();
	   
	     if( todaydate=='')
	        {
	    	 	
	    	 	$("#today_date").after('<span style="color: red;white-space:nowrap;" class="error">Please Select Date.....</span>');
	    	
	    	 	hasError = true;
	        }
	    if(hasError == true){
	    	return false; 
	    }
	    
		    else
		    {
		    	
		    	
		    	$('#appointments').load("loadAppointments.action?todaydate="+todaydate);
		    	$('#appointments').show();
		    	
		    	$('#appointments_by_date').hide();
		    	
		    	
		    
		    	return true; 
		    }
			
			
		});
 */	
});
</script>
<script>
	
	
	jQuery(function(){
		 
		
		 $('#salesperson_state').autocomplete('CityautoCompleteAction.action?variable=state');
			$('#salesperson_city').autocomplete('CityautoCompleteAction.action',
					
					{
				  extraParams:
					
				        {
					  businessState: function () {
				                return $("#salesperson_state").val();
				            },
				            
				            
				            variable: function () {
				                return "city";
				            }
				
				        }
					}
			
			
			);
			
			/*area starts here  */
			
			
			
			/*Start of area auto complete  */
		$('#salesperson_area').autocomplete('CityautoCompleteAction.action',
					
					{
				  extraParams:
					
				        {
					  businesCity: function () {
				                return $("#salesperson_city").val();
				            },
				            
				            
				            variable: function () {
				                return "area";
				            }

				        }
					}
			
			
			);
	});
			</script>
	<script>
	jQuery(function(){
		 
		
	
		 $('#telecaller_state').autocomplete('CityautoCompleteAction.action?variable=state');
			$('#telecaller_city').autocomplete('CityautoCompleteAction.action',
					
					{
				  extraParams:
					
				        {
					  businessState: function () {
				                return $("#telecaller_state").val();
				            },
				            
				            
				            variable: function () {
				                return "city";
				            }
				
				        }
					}
			
			
			);
			
			/*area starts here  */
			
			
			
			/*Start of area auto complete  */
		$('#telecaller_area').autocomplete('CityautoCompleteAction.action',
					
					{
				  extraParams:
					
				        {
					  businesCity: function () {
				                return $("#telecaller_city").val();
				            },
				            
				            
				            variable: function () {
				                return "area";
				            }

				        }
					}
			
			
			);
	});
</script>
<script>
$(document).ready(function()
{
		
		$(function()
		{
		  $( ".reportTodate" ).datepicker();
		});
		
		$(function()
		{
		  $( ".reportFromdate" ).datepicker();
		});
});
</script>

</head>

<body>
<div id="appointmentform_div"></div>
<div id="salesPersonContactDetails"></div>
<div class="top_bg">
	<div class="topbg_arabicner">
   	  <div class="topbg_arabicner_left">
        	<div class="topbg_arabicner_left_top">
            	SALES TRACKING
            </div>
            <div class="topbg_arabicner_left_bottom">
             <span id="date_time"></span>
            <script type="text/javascript">window.onload = date_time('date_time');</script>
          </div>
      </div>
      <div class="topbg_arabicner_right">
        	<div class="topbg_arabicner_right_left">
       	  <img src="images/dialus_small.png" width="28" height="27" /> </div>
            <div class="topbg_arabicner_right_right">
            9666 888 888
            </div>
      </div>
    </div>
</div>
<div style="clear:both"></div>
<div class="wrapper">
<div class="logopart">
	<div class="logopart_left">
    <img src="images/logo.jpg" width="225" height="90" /> 
    </div>
    <div class="logopart_right">
     
	
    	<ul>
        
        <li>About Us</li>
         <li>|</li>
        <li>Services</li>
         <li>|</li>
        <li>Gallery</li>
          <li>|</li>
        <li>Products</li>
          <li>|</li>
        <li>Contact Us</li>
        </ul>
    </div>
</div>
<s:if test="%{#request.success_msg!=null && #request.success_msg!=''}">
		<div style="text-align: center;width:980px;" id="success_msg"> <font style="color: red;font-family: georgia;font-weight: bold;"><s:property value="%{#request.success_msg}"/></font></div>
	</s:if>
<div class="contentpart">
	<div class="content_left">
    	
        <div id="login_links">
	        <div class="content_left_headding">
		  <div class="content_left_text">
            Menu
          </div>
      </div>
	       <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="appointments_link" >Today Appointments</a>
	        	</div>
	       </div>
	        <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="pending_appointments_link" >Pending Appointments</a>
	        	</div>
	       </div>
	       <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="rejected_appointments_link" >Rejected Appointments</a>
	        	</div>
	       </div>
	       <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="createuser_link" >Create Users</a>
	        	</div>
	       </div>
	       <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="deleteuser_link" >Delete Users</a>
	        	</div>
	       </div>
	        <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="edituser_link" >Edit Users</a>
	        	</div>
	       </div>
	         <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="telecallerreport_link" >TeleSales Persons Daily Calls Report</a>
	        	</div>
	       </div>
	         <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="salesreport_link" >Business Dev Officers Report</a>
	        	</div>
	       </div>
	        <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="datewisereport_link" >Date Wise Reports</a>
	        	</div>
	       </div>
	       
	       <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="business_link" >Business Details</a>
	        	</div>
	       </div>
	     
	      <!--  <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="postnews_link" >Post News</a>
	        	</div>
	       </div>
	       <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="inbox_link" >Inbox</a>
	        	</div>
	       </div>
	       <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="sentitems_link" >Sent Items</a>
	        	</div>
	       </div>
	      
	        <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="yourprofile_link" >Your Profile</a>
	        	</div>
	       </div>
	        <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="settings_link">Settings</a>
	        	</div>
	       </div>
	        <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="reports_link" >Reports</a>
	        	</div>
	       </div>
	       --> <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a href="LogOut.action"style="cursor: pointer;text-decoration: none;color:#f30909;" id="logout_link">Log Out</a>
	        	</div>
	       </div>
      </div>
      
    </div>
    <div class="content_right">
    		 <!-- <div id="appointmentform_div"></div> -->
    		  <div id="appointments"></div>
    		  
    		  <div id="appointments_by_date">
    		  
    		  <div class="main_div_middle_textfields_main" style="width:600px;">
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:150px; text-align:right; margin-left:30px;">
								       	   		<b style="color:red">&lowast;</b>Select Date :
								            </div>
                                            
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px; width:219px;">
								           		<%--  <s:textfield name="reportTodate" id="reportTodate"  cssClass="reportTodate"></s:textfield> --%>
								           		 <s:textfield name="todaydate" id="today_date"  cssClass="reportTodate123" onfocus="show_date()"></s:textfield>
								            </div>
								            
								            <div class="main_div_middle_textfields_main_right" style="margin-left:5px; width:80px;">
								           		<%--  <s:textfield name="reportTodate" id="reportTodate"  cssClass="reportTodate"></s:textfield> --%>
								           		 <s:submit id="submit_date"  onclick="submit_button_clicked()"></s:submit>
								            </div>
							</div>
    		  
    		  
    		  
    		  
    		  </div>
	     <!--  <div id="appointmentform_div"></div> -->
	       <div id="rejected_appointments_div"></div>
	        <div id="pending_appointments_div"></div>
	        
	   	  <div class="banner_part" id="banner_div">
	   	  WelCome To Sales Tracking Admin Panel
	        	
	            
	            <div class="banner_part">
	             </div>
	            
	      </div>
	     
	        
	      <div id="createuser_div">
	      
		       <div class="main_div_middle">
			         
		                       	<div class="appointmentform_left_left">
		                         		  Select User Type:
		                        </div>
		                            
		                   		<div class="appointmentform_left_right">
		                     			<select id="persontype" onchange="selectUser()">
					      					<option value="123">-----Select-------</option>
						      				<option value="salesperson">Business Dev Officer</option>
						      				<option value="telecaller">Tele Sales</option>
	      								</select>
		                   		</div>
			         
		           </div>
	      
	      				
	        </div>
				       <div id="salesperson_div">
						      	<s:form action="createUsersAction" theme="simple">
								    	<s:hidden name="objUsersOperationsDTO.usertype" value="salesperson"></s:hidden>
								        <div class="main_div_middle">
								         <div class="main_div_middle_textfields_main" style="text-align:center;background: #008ABB;width: 100%;color:#fff;padding: 8px 0px;margin-top:-10px;border-top-left-radius:10px;border-top-right-radius:10px;font-family: georgia;">
											           Please Enter Business Dev Officer  Details
											        </div>
									        <div class="appointmentform_left1" style="margin-left:200px;">
                                	<div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                         <b style="color:red">&lowast;</b>Name :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objUsersOperationsDTO.userName" maxlength="45" id="salesperson_name"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                         <b style="color:red">&lowast;</b>State :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userState" maxlength="40"    id="salesperson_state"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b> City :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userCity" maxlength="40"   id="salesperson_city"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                     <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>Area :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objUsersOperationsDTO.userArea" maxlength="40"    id="salesperson_area"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>Mobile :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objUsersOperationsDTO.userMobile" maxlength="10"    id="salesperson_mobile"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                       <div id="mbnoExist"></div>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        Mobile2 :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userMobile1" maxlength="10"     id="salesperson_mobile1"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                     Mail Id :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userMailId" maxlength="40"    id="salesperson_mailid"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Password :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:password  name="objUsersOperationsDTO.userPassword" maxlength="8"    id="salesperson_password"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:password>
                                        </div>
                                    </div>
                                      <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>Confirm Password
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:password    maxlength="8"    id="salesperson_confirmpassword"   cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:password>
                                        </div>
                                    </div>
                                      <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                    
                                        </div>
                                        
                                        <div class="appointmentform_left_right" style="float: right;">
                                      <s:submit value="Create User" id="createSalesperson_but"></s:submit>
                                        </div>
                                    </div>
                                    </div>
									            
									            
									          
									        </div>
								     
								     </s:form>
							
				      </div>
				      
				      <div id="telecaller_div">
						      	<s:form action="createUsersAction" theme="simple">
								    	<s:hidden name="objUsersOperationsDTO.usertype" value="telecaller"></s:hidden>
								        <div class="main_div_middle">
								         <div class="main_div_middle_textfields_main" style="text-align:center;background: #008ABB;width: 100%;color:#fff;padding: 8px 0px;margin-top:-10px;border-top-left-radius:10px;border-top-right-radius:10px;font-family: georgia;">
											           Please Enter Tele Sales Person  Details
											        </div>
									        <div class="appointmentform_left1" style="margin-left:200px;">
                                	<div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>Name :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objUsersOperationsDTO.userName" maxlength="90" id="telecaller_name"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                   <%--  <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       Telecaller State :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userState" maxlength="90"    id="telecaller_state"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                      Telecaller  City :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userCity" maxlength="90"   id="telecaller_city"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                     <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       Telecaller Area :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objUsersOperationsDTO.userArea" maxlength="50"   id="telecaller_area"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div> --%>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b> Mobile :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objUsersOperationsDTO.userMobile" maxlength="10"    id="telecaller_mobile"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                       <div id="telecallermbnoExist"></div>
                                        </div>
                                    </div>
                                    <%-- <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        Telecaller Mobile2 :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userMobile1" maxlength="10"     id="telecaller_mobile1"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div> --%>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                      Mail Id :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userMailId" maxlength="40"    id="telecaller_mailid"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Password :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:password  name="objUsersOperationsDTO.userPassword"  maxlength="8"    id="telecaller_password"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:password>
                                        </div>
                                    </div>
                                      <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>Confirm Password
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:password   maxlength="8"    id="telecaller_confirmpassword"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:password>
                                        </div>
                                    </div>
                                      <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                    
                                        </div>
                                        
                                        <div class="appointmentform_left_right" style="float: right;">
                                      <s:submit value="Create User" id="createTelecaller_but"></s:submit>
                                        </div>
                                    </div>
                                    </div>
									            
									            
									          
									        </div>
								     
								     </s:form>
							
				      </div>
				      
	      
	    
	      <div id="deleteuser_div">
	           <div class="main_div_middle">
		         
	                       	<div class="appointmentform_left_left">
	                         		 Select User Type:
	                        </div>
	                            
	                   		<div class="appointmentform_left_right">
	                     		<select id="deletepersontype" onchange="selectDeleteUser()">
									<option value="123">-----Select-------</option>
									<option value="salesperson">Business Dev Officer</option>
									<option value="telecaller">Tele Sales</option>
								</select>
	                   		</div>
		         
	           </div>
          
	      </div>
	      <div id="deletesalespersons_div"></div>
	      <div id="deletetelecallers_div"></div>
	      <div id="business_div"></div>
	      <div id="edituser_div">
	      
	      <div class="main_div_middle">
		         
	                       	<div class="appointmentform_left_left">
	                         		   Select User Type:
	                        </div>
	                            
	                   		<div class="appointmentform_left_right">
	                     		<select id="editpersontype" onchange="selectEditUser()">
				      					<option value="123">-----Select-------</option>
					      				<option value="salesperson">Business Dev Officer</option>
					      				<option value="telecaller">Tele Sales</option>
	      						</select>
	                   		</div>
		         
	           </div>
	    </div>
	      <div id="editsalespersons_div"></div>
	      <div id="edittelecallers_div"></div>
	       <div id="editsalespersonsdetails_div"></div>
	      <div id="edittelecallersdetails_div"></div>
	       <div id="telecallerreport_div">
	       
			       <div class="main_div_middle" style="width:610px;">
				         
					                       	
					                            
					                   		<div class="appointmentform_left_right" style="width:40%;">
					                   			<div class="appointmentform_left_left" style="margin-bottom: 10px;">
					                         		   Select Report Type:
					                        	</div>
					                     		<select id="reporttype">
								      					<option value="123">-----Select-------</option>
									      				<option value="daily">Daily Report</option>
									      				<option value="weekly">Weekly Report</option>
									      				<option value="monthly">Monthly Report</option>
					      						</select>
					                   		</div>
					                   		
					                            
					                   		<div class="appointmentform_left_right" style="width:50%;">
						                   		<div class="appointmentform_left_left" style="width:180px;margin-bottom: 10px;">
						                         		   Select TeleSales Person Name:
						                        </div>
					                     		<select id="telecallername">
								      					<option value="123">-----Select-------</option>
								      					<s:if test="%{telecallerNames.size!=0}">
										      				<s:iterator value="telecallerNames" id="a">
										      				<option value="<s:property/>"><s:property/></option>
										      				</s:iterator>
									      				</s:if>
									      				<s:else><font color="red" face="georgia"><b>Please Create TeleSales Persons</b></font></s:else>
					      						</select>
					                   		</div>
					                   		
					                   		
					                   		<div class="appointmentform_left_left" style="margin-left: 130px;margin-top: 20px;">
						                         		   <input type="button" id="getTelecallerReport"  value="Get Report"/>
						                        </div>
				         
			          		 </div>
	       
	       
	       
	       
	       
	       </div>
	        <div id="calllist_div"></div>
	        <div id="saleslist_div"></div>
	        
	         <div id="salesreport_div">
	         
	             <div class="main_div_middle" style="width:610px;">
		         
			                       	
			                            
			                   		<div class="appointmentform_left_right" style="width:40%;">
			                   			<div class="appointmentform_left_left" style="margin-bottom: 10px;">
			                         		   Select Report Type:
			                        	</div>
			                     		<select id="salesreporttype">
						      					<option value="123">-----Select-------</option>
							      				<option value="daily">Daily Report</option>
							      				<option value="weekly">Weekly Report</option>
							      				<option value="monthly">Monthly Report</option>
			      						</select>
			                   		</div>
			                   		
			                            
			                   		<div class="appointmentform_left_right" style="width:50%;">
				                   		<div class="appointmentform_left_left" style="width:180px;margin-bottom: 10px;white-space: nowrap;">
				                         		   Select Business Dev Officer Name:
				                        </div>
			                     		<select id="salespersonnamereport">
						      					<option value="123">-----Select-------</option>
						      					<s:if test="%{salesPersons.size!=0}">
								      				<s:iterator value="salesPersons" id="a">
								      				<option value="<s:property value="#a.salesPersonContactNo"/>"><s:property value="#a.salesPersonName"/></option>
								      				</s:iterator>
							      				</s:if>
							      				<s:else><font color="red" face="georgia"><b>Please Create Business Dev Officers</b></font></s:else>
			      						</select>
			                   		</div>
			                   		
			                   		
			                   		<div class="appointmentform_left_left" style="margin-left: 130px;margin-top: 20px;">
				                         		   <input type="button" id="getSalesPersonReport_but"  value="Get Report"/>
				                        </div>
		         
	          		 </div>
	         
	         
	         
	         
	         </div>
	       
	       
	       <div id="datewisereport_div">
	       			<div class="main_div_middle">
	       				<div class="main_div_middle_textfields_main" style="text-align:center;background: #008ABB;width: 100%;color:#fff;padding: 8px 0px;margin-top:-10px;border-top-left-radius:10px;border-top-right-radius:10px;">
								        Date Wise Reports
						</div>
  						
  							<s:hidden name="reportUserType"  id="reportUserType"></s:hidden>
  							<div class="main_div_middle_textfields_main" style="width:600px;">
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:180px; text-align:right; margin-left:60px;">
								       	   		<b style="color:red">&lowast;</b>From Date :
								            </div>
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px;">
								           		 <s:textfield name="reportFromdate" id="reportFromdate"   cssClass="reportFromdate"></s:textfield>
								            </div>
							</div>
							<div class="main_div_middle_textfields_main" style="width:600px;">
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:180px; text-align:right; margin-left:60px;">
								       	   		<b style="color:red">&lowast;</b>To Date :
								            </div>
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px;">
								           		 <s:textfield name="reportTodate" id="reportTodate"  cssClass="reportTodate"></s:textfield>
								            </div>
							</div>
							<div class="main_div_middle_textfields_main" style="width:600px;">
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:465px; text-align:right;float: left;">
								       	   		Business Dev Officer: <input type="radio" onclick="changeTextVal('sales')" name="reportUserTypeCheck"  id="reportUserType1" value="sales" />
								       	   		Tele Sales Person:	<input type="radio"  onclick="changeTextVal1('telecaller')"  name="reportUserTypeCheck"   id="reportUserType2" value="telecaller" />
								            </div>
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px;">
								           		
								            </div>
							</div>
							<div class="main_div_middle_textfields_main" style="width:600px;">
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:180px; text-align:right; margin-left:60px;">
								       	   		<b style="color:red">&lowast;</b>Enter User Name :
								            </div>
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px;">
								           		<s:textfield name="reportUserName"   id="reportUserName"> </s:textfield>
								            </div>
							</div>
	  						<div class="main_div_middle_textfields_main" style="width:600px;">
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:180px; text-align:right; margin-left:60px;color: white;">
								            .
								            </div>
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px;">
								           		<input type="button" value="get Report" id="getReportBut"/>
								            </div>
							</div>
	  						
	  					
	  						
	  						
  						
	       				
	       </div>
	   </div>
	     <div id="datewiseReportsResults_div"></div>
  </div>
</div>
</div>
<div class="fotter">
	<div class="fotter_arabicner">
     All Rights Reserved - Copyright ï½© Dialus.com 2010-2013
    </div>
</div>
</body>
</html>
